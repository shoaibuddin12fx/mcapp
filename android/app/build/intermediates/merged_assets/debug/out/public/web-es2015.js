(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["web"],{

/***/ "./node_modules/@capacitor-community/sqlite/dist/esm/web.js":
/*!******************************************************************!*\
  !*** ./node_modules/@capacitor-community/sqlite/dist/esm/web.js ***!
  \******************************************************************/
/*! exports provided: CapacitorSQLiteWeb */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapacitorSQLiteWeb", function() { return CapacitorSQLiteWeb; });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/index.js");

class CapacitorSQLiteWeb extends _capacitor_core__WEBPACK_IMPORTED_MODULE_0__["WebPlugin"] {
    constructor() {
        super();
        this.sqliteEl = null;
        this.isStoreOpen = false;
        this.sqliteEl = document.querySelector('jeep-sqlite');
        if (this.sqliteEl != null) {
            this.sqliteEl.addEventListener('jeepSqliteImportProgress', (event) => {
                this.notifyListeners('sqliteImportProgressEvent', event.detail);
            });
            this.sqliteEl.addEventListener('jeepSqliteExportProgress', (event) => {
                this.notifyListeners('sqliteExportProgressEvent', event.detail);
            });
            if (!this.isStoreOpen)
                this.sqliteEl.isStoreOpen().then((isOpen) => {
                    this.isStoreOpen = isOpen;
                });
        }
        else {
            console.log(`$$$$$$ this.sqliteEl is null $$$$$$`);
        }
    }
    async echo(options) {
        if (this.sqliteEl != null) {
            const echo = await this.sqliteEl.echo(options);
            return echo;
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async isSecretStored() {
        throw this.unimplemented('Not implemented on web.');
    }
    async setEncryptionSecret(options) {
        console.log('setEncryptionSecret', options);
        throw this.unimplemented('Not implemented on web.');
    }
    async changeEncryptionSecret(options) {
        console.log('changeEncryptionSecret', options);
        throw this.unimplemented('Not implemented on web.');
    }
    async createConnection(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    await this.sqliteEl.createConnection(options);
                    return Promise.resolve();
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async open(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    await this.sqliteEl.open(options);
                    return Promise.resolve();
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async closeConnection(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    await this.sqliteEl.closeConnection(options);
                    return Promise.resolve();
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async getVersion(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.getVersion(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async checkConnectionsConsistency(options) {
        if (this.sqliteEl != null) {
            try {
                const ret = await this.sqliteEl.checkConnectionsConsistency(options);
                return Promise.resolve(ret);
            }
            catch (err) {
                return Promise.reject(`${err}`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async close(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    await this.sqliteEl.close(options);
                    return Promise.resolve();
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async execute(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.execute(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async executeSet(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.executeSet(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async run(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.run(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async query(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.query(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async isDBExists(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.isDBExists(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async isDBOpen(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.isDBOpen(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async isDatabase(options) {
        if (this.sqliteEl != null) {
            if (!this.isStoreOpen)
                this.isStoreOpen = await this.sqliteEl.isStoreOpen();
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.isDatabase(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async isTableExists(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.isTableExists(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async deleteDatabase(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    await this.sqliteEl.deleteDatabase(options);
                    return Promise.resolve();
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async isJsonValid(options) {
        if (this.sqliteEl != null) {
            if (!this.isStoreOpen)
                this.isStoreOpen = await this.sqliteEl.isStoreOpen();
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.isJsonValid(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async importFromJson(options) {
        if (this.sqliteEl != null) {
            if (!this.isStoreOpen)
                this.isStoreOpen = await this.sqliteEl.isStoreOpen();
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.importFromJson(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async exportToJson(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.exportToJson(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async createSyncTable(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.createSyncTable(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async setSyncDate(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    await this.sqliteEl.setSyncDate(options);
                    return Promise.resolve();
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async getSyncDate(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.getSyncDate(options);
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async addUpgradeStatement(options) {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    await this.sqliteEl.addUpgradeStatement(options);
                    return Promise.resolve();
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async copyFromAssets() {
        if (this.sqliteEl != null) {
            if (this.isStoreOpen) {
                try {
                    await this.sqliteEl.copyFromAssets();
                    return Promise.resolve();
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async getDatabaseList() {
        if (this.sqliteEl != null) {
            if (!this.isStoreOpen)
                this.isStoreOpen = await this.sqliteEl.isStoreOpen();
            if (this.isStoreOpen) {
                try {
                    const ret = await this.sqliteEl.getDatabaseList();
                    return Promise.resolve(ret);
                }
                catch (err) {
                    return Promise.reject(`${err}`);
                }
            }
            else {
                return Promise.reject(`Store "jeepSqliteStore" failed to open`);
            }
        }
        else {
            throw this.unimplemented('Not implemented on web.');
        }
    }
    async addSQLiteSuffix(options) {
        console.log('addSQLiteSuffix', options);
        throw this.unimplemented('Not implemented on web.');
    }
    async deleteOldDatabases(options) {
        console.log('deleteOldDatabases', options);
        throw this.unimplemented('Not implemented on web.');
    }
}
//# sourceMappingURL=web.js.map

/***/ })

}]);
//# sourceMappingURL=web-es2015.js.map