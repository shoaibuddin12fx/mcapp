function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["web"], {
  /***/
  "./node_modules/@capacitor-community/sqlite/dist/esm/web.js":
  /*!******************************************************************!*\
    !*** ./node_modules/@capacitor-community/sqlite/dist/esm/web.js ***!
    \******************************************************************/

  /*! exports provided: CapacitorSQLiteWeb */

  /***/
  function node_modulesCapacitorCommunitySqliteDistEsmWebJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CapacitorSQLiteWeb", function () {
      return CapacitorSQLiteWeb;
    });
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/index.js");

    var CapacitorSQLiteWeb = /*#__PURE__*/function (_capacitor_core__WEBP) {
      _inherits(CapacitorSQLiteWeb, _capacitor_core__WEBP);

      var _super = _createSuper(CapacitorSQLiteWeb);

      function CapacitorSQLiteWeb() {
        var _this;

        _classCallCheck(this, CapacitorSQLiteWeb);

        _this = _super.call(this);
        _this.sqliteEl = null;
        _this.isStoreOpen = false;
        _this.sqliteEl = document.querySelector('jeep-sqlite');

        if (_this.sqliteEl != null) {
          _this.sqliteEl.addEventListener('jeepSqliteImportProgress', function (event) {
            _this.notifyListeners('sqliteImportProgressEvent', event.detail);
          });

          _this.sqliteEl.addEventListener('jeepSqliteExportProgress', function (event) {
            _this.notifyListeners('sqliteExportProgressEvent', event.detail);
          });

          if (!_this.isStoreOpen) _this.sqliteEl.isStoreOpen().then(function (isOpen) {
            _this.isStoreOpen = isOpen;
          });
        } else {
          console.log("$$$$$$ this.sqliteEl is null $$$$$$");
        }

        return _this;
      }

      _createClass(CapacitorSQLiteWeb, [{
        key: "echo",
        value: function () {
          var _echo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(options) {
            var _echo2;

            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context.next = 7;
                      break;
                    }

                    _context.next = 3;
                    return this.sqliteEl.echo(options);

                  case 3:
                    _echo2 = _context.sent;
                    return _context.abrupt("return", _echo2);

                  case 7:
                    throw this.unimplemented('Not implemented on web.');

                  case 8:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));

          function echo(_x) {
            return _echo.apply(this, arguments);
          }

          return echo;
        }()
      }, {
        key: "isSecretStored",
        value: function () {
          var _isSecretStored = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    throw this.unimplemented('Not implemented on web.');

                  case 1:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));

          function isSecretStored() {
            return _isSecretStored.apply(this, arguments);
          }

          return isSecretStored;
        }()
      }, {
        key: "setEncryptionSecret",
        value: function () {
          var _setEncryptionSecret = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(options) {
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    console.log('setEncryptionSecret', options);
                    throw this.unimplemented('Not implemented on web.');

                  case 2:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));

          function setEncryptionSecret(_x2) {
            return _setEncryptionSecret.apply(this, arguments);
          }

          return setEncryptionSecret;
        }()
      }, {
        key: "changeEncryptionSecret",
        value: function () {
          var _changeEncryptionSecret = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(options) {
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    console.log('changeEncryptionSecret', options);
                    throw this.unimplemented('Not implemented on web.');

                  case 2:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));

          function changeEncryptionSecret(_x3) {
            return _changeEncryptionSecret.apply(this, arguments);
          }

          return changeEncryptionSecret;
        }()
      }, {
        key: "createConnection",
        value: function () {
          var _createConnection = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(options) {
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context5.next = 16;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context5.next = 13;
                      break;
                    }

                    _context5.prev = 2;
                    _context5.next = 5;
                    return this.sqliteEl.createConnection(options);

                  case 5:
                    return _context5.abrupt("return", Promise.resolve());

                  case 8:
                    _context5.prev = 8;
                    _context5.t0 = _context5["catch"](2);
                    return _context5.abrupt("return", Promise.reject("".concat(_context5.t0)));

                  case 11:
                    _context5.next = 14;
                    break;

                  case 13:
                    return _context5.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 14:
                    _context5.next = 17;
                    break;

                  case 16:
                    throw this.unimplemented('Not implemented on web.');

                  case 17:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this, [[2, 8]]);
          }));

          function createConnection(_x4) {
            return _createConnection.apply(this, arguments);
          }

          return createConnection;
        }()
      }, {
        key: "open",
        value: function () {
          var _open = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(options) {
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context6.next = 16;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context6.next = 13;
                      break;
                    }

                    _context6.prev = 2;
                    _context6.next = 5;
                    return this.sqliteEl.open(options);

                  case 5:
                    return _context6.abrupt("return", Promise.resolve());

                  case 8:
                    _context6.prev = 8;
                    _context6.t0 = _context6["catch"](2);
                    return _context6.abrupt("return", Promise.reject("".concat(_context6.t0)));

                  case 11:
                    _context6.next = 14;
                    break;

                  case 13:
                    return _context6.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 14:
                    _context6.next = 17;
                    break;

                  case 16:
                    throw this.unimplemented('Not implemented on web.');

                  case 17:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this, [[2, 8]]);
          }));

          function open(_x5) {
            return _open.apply(this, arguments);
          }

          return open;
        }()
      }, {
        key: "closeConnection",
        value: function () {
          var _closeConnection = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(options) {
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context7.next = 16;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context7.next = 13;
                      break;
                    }

                    _context7.prev = 2;
                    _context7.next = 5;
                    return this.sqliteEl.closeConnection(options);

                  case 5:
                    return _context7.abrupt("return", Promise.resolve());

                  case 8:
                    _context7.prev = 8;
                    _context7.t0 = _context7["catch"](2);
                    return _context7.abrupt("return", Promise.reject("".concat(_context7.t0)));

                  case 11:
                    _context7.next = 14;
                    break;

                  case 13:
                    return _context7.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 14:
                    _context7.next = 17;
                    break;

                  case 16:
                    throw this.unimplemented('Not implemented on web.');

                  case 17:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this, [[2, 8]]);
          }));

          function closeConnection(_x6) {
            return _closeConnection.apply(this, arguments);
          }

          return closeConnection;
        }()
      }, {
        key: "getVersion",
        value: function () {
          var _getVersion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context8.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context8.next = 14;
                      break;
                    }

                    _context8.prev = 2;
                    _context8.next = 5;
                    return this.sqliteEl.getVersion(options);

                  case 5:
                    ret = _context8.sent;
                    return _context8.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context8.prev = 9;
                    _context8.t0 = _context8["catch"](2);
                    return _context8.abrupt("return", Promise.reject("".concat(_context8.t0)));

                  case 12:
                    _context8.next = 15;
                    break;

                  case 14:
                    return _context8.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context8.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this, [[2, 9]]);
          }));

          function getVersion(_x7) {
            return _getVersion.apply(this, arguments);
          }

          return getVersion;
        }()
      }, {
        key: "checkConnectionsConsistency",
        value: function () {
          var _checkConnectionsConsistency = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context9.next = 13;
                      break;
                    }

                    _context9.prev = 1;
                    _context9.next = 4;
                    return this.sqliteEl.checkConnectionsConsistency(options);

                  case 4:
                    ret = _context9.sent;
                    return _context9.abrupt("return", Promise.resolve(ret));

                  case 8:
                    _context9.prev = 8;
                    _context9.t0 = _context9["catch"](1);
                    return _context9.abrupt("return", Promise.reject("".concat(_context9.t0)));

                  case 11:
                    _context9.next = 14;
                    break;

                  case 13:
                    throw this.unimplemented('Not implemented on web.');

                  case 14:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this, [[1, 8]]);
          }));

          function checkConnectionsConsistency(_x8) {
            return _checkConnectionsConsistency.apply(this, arguments);
          }

          return checkConnectionsConsistency;
        }()
      }, {
        key: "close",
        value: function () {
          var _close = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(options) {
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context10.next = 16;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context10.next = 13;
                      break;
                    }

                    _context10.prev = 2;
                    _context10.next = 5;
                    return this.sqliteEl.close(options);

                  case 5:
                    return _context10.abrupt("return", Promise.resolve());

                  case 8:
                    _context10.prev = 8;
                    _context10.t0 = _context10["catch"](2);
                    return _context10.abrupt("return", Promise.reject("".concat(_context10.t0)));

                  case 11:
                    _context10.next = 14;
                    break;

                  case 13:
                    return _context10.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 14:
                    _context10.next = 17;
                    break;

                  case 16:
                    throw this.unimplemented('Not implemented on web.');

                  case 17:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this, [[2, 8]]);
          }));

          function close(_x9) {
            return _close.apply(this, arguments);
          }

          return close;
        }()
      }, {
        key: "execute",
        value: function () {
          var _execute = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context11.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context11.next = 14;
                      break;
                    }

                    _context11.prev = 2;
                    _context11.next = 5;
                    return this.sqliteEl.execute(options);

                  case 5:
                    ret = _context11.sent;
                    return _context11.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context11.prev = 9;
                    _context11.t0 = _context11["catch"](2);
                    return _context11.abrupt("return", Promise.reject("".concat(_context11.t0)));

                  case 12:
                    _context11.next = 15;
                    break;

                  case 14:
                    return _context11.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context11.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this, [[2, 9]]);
          }));

          function execute(_x10) {
            return _execute.apply(this, arguments);
          }

          return execute;
        }()
      }, {
        key: "executeSet",
        value: function () {
          var _executeSet = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context12.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context12.next = 14;
                      break;
                    }

                    _context12.prev = 2;
                    _context12.next = 5;
                    return this.sqliteEl.executeSet(options);

                  case 5:
                    ret = _context12.sent;
                    return _context12.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context12.prev = 9;
                    _context12.t0 = _context12["catch"](2);
                    return _context12.abrupt("return", Promise.reject("".concat(_context12.t0)));

                  case 12:
                    _context12.next = 15;
                    break;

                  case 14:
                    return _context12.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context12.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this, [[2, 9]]);
          }));

          function executeSet(_x11) {
            return _executeSet.apply(this, arguments);
          }

          return executeSet;
        }()
      }, {
        key: "run",
        value: function () {
          var _run = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee13(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee13$(_context13) {
              while (1) {
                switch (_context13.prev = _context13.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context13.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context13.next = 14;
                      break;
                    }

                    _context13.prev = 2;
                    _context13.next = 5;
                    return this.sqliteEl.run(options);

                  case 5:
                    ret = _context13.sent;
                    return _context13.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context13.prev = 9;
                    _context13.t0 = _context13["catch"](2);
                    return _context13.abrupt("return", Promise.reject("".concat(_context13.t0)));

                  case 12:
                    _context13.next = 15;
                    break;

                  case 14:
                    return _context13.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context13.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context13.stop();
                }
              }
            }, _callee13, this, [[2, 9]]);
          }));

          function run(_x12) {
            return _run.apply(this, arguments);
          }

          return run;
        }()
      }, {
        key: "query",
        value: function () {
          var _query = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee14(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee14$(_context14) {
              while (1) {
                switch (_context14.prev = _context14.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context14.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context14.next = 14;
                      break;
                    }

                    _context14.prev = 2;
                    _context14.next = 5;
                    return this.sqliteEl.query(options);

                  case 5:
                    ret = _context14.sent;
                    return _context14.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context14.prev = 9;
                    _context14.t0 = _context14["catch"](2);
                    return _context14.abrupt("return", Promise.reject("".concat(_context14.t0)));

                  case 12:
                    _context14.next = 15;
                    break;

                  case 14:
                    return _context14.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context14.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context14.stop();
                }
              }
            }, _callee14, this, [[2, 9]]);
          }));

          function query(_x13) {
            return _query.apply(this, arguments);
          }

          return query;
        }()
      }, {
        key: "isDBExists",
        value: function () {
          var _isDBExists = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee15(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee15$(_context15) {
              while (1) {
                switch (_context15.prev = _context15.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context15.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context15.next = 14;
                      break;
                    }

                    _context15.prev = 2;
                    _context15.next = 5;
                    return this.sqliteEl.isDBExists(options);

                  case 5:
                    ret = _context15.sent;
                    return _context15.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context15.prev = 9;
                    _context15.t0 = _context15["catch"](2);
                    return _context15.abrupt("return", Promise.reject("".concat(_context15.t0)));

                  case 12:
                    _context15.next = 15;
                    break;

                  case 14:
                    return _context15.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context15.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context15.stop();
                }
              }
            }, _callee15, this, [[2, 9]]);
          }));

          function isDBExists(_x14) {
            return _isDBExists.apply(this, arguments);
          }

          return isDBExists;
        }()
      }, {
        key: "isDBOpen",
        value: function () {
          var _isDBOpen = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee16(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee16$(_context16) {
              while (1) {
                switch (_context16.prev = _context16.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context16.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context16.next = 14;
                      break;
                    }

                    _context16.prev = 2;
                    _context16.next = 5;
                    return this.sqliteEl.isDBOpen(options);

                  case 5:
                    ret = _context16.sent;
                    return _context16.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context16.prev = 9;
                    _context16.t0 = _context16["catch"](2);
                    return _context16.abrupt("return", Promise.reject("".concat(_context16.t0)));

                  case 12:
                    _context16.next = 15;
                    break;

                  case 14:
                    return _context16.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context16.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context16.stop();
                }
              }
            }, _callee16, this, [[2, 9]]);
          }));

          function isDBOpen(_x15) {
            return _isDBOpen.apply(this, arguments);
          }

          return isDBOpen;
        }()
      }, {
        key: "isDatabase",
        value: function () {
          var _isDatabase = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee17(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee17$(_context17) {
              while (1) {
                switch (_context17.prev = _context17.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context17.next = 21;
                      break;
                    }

                    if (this.isStoreOpen) {
                      _context17.next = 5;
                      break;
                    }

                    _context17.next = 4;
                    return this.sqliteEl.isStoreOpen();

                  case 4:
                    this.isStoreOpen = _context17.sent;

                  case 5:
                    if (!this.isStoreOpen) {
                      _context17.next = 18;
                      break;
                    }

                    _context17.prev = 6;
                    _context17.next = 9;
                    return this.sqliteEl.isDatabase(options);

                  case 9:
                    ret = _context17.sent;
                    return _context17.abrupt("return", Promise.resolve(ret));

                  case 13:
                    _context17.prev = 13;
                    _context17.t0 = _context17["catch"](6);
                    return _context17.abrupt("return", Promise.reject("".concat(_context17.t0)));

                  case 16:
                    _context17.next = 19;
                    break;

                  case 18:
                    return _context17.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 19:
                    _context17.next = 22;
                    break;

                  case 21:
                    throw this.unimplemented('Not implemented on web.');

                  case 22:
                  case "end":
                    return _context17.stop();
                }
              }
            }, _callee17, this, [[6, 13]]);
          }));

          function isDatabase(_x16) {
            return _isDatabase.apply(this, arguments);
          }

          return isDatabase;
        }()
      }, {
        key: "isTableExists",
        value: function () {
          var _isTableExists = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee18(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee18$(_context18) {
              while (1) {
                switch (_context18.prev = _context18.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context18.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context18.next = 14;
                      break;
                    }

                    _context18.prev = 2;
                    _context18.next = 5;
                    return this.sqliteEl.isTableExists(options);

                  case 5:
                    ret = _context18.sent;
                    return _context18.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context18.prev = 9;
                    _context18.t0 = _context18["catch"](2);
                    return _context18.abrupt("return", Promise.reject("".concat(_context18.t0)));

                  case 12:
                    _context18.next = 15;
                    break;

                  case 14:
                    return _context18.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context18.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context18.stop();
                }
              }
            }, _callee18, this, [[2, 9]]);
          }));

          function isTableExists(_x17) {
            return _isTableExists.apply(this, arguments);
          }

          return isTableExists;
        }()
      }, {
        key: "deleteDatabase",
        value: function () {
          var _deleteDatabase = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee19(options) {
            return regeneratorRuntime.wrap(function _callee19$(_context19) {
              while (1) {
                switch (_context19.prev = _context19.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context19.next = 16;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context19.next = 13;
                      break;
                    }

                    _context19.prev = 2;
                    _context19.next = 5;
                    return this.sqliteEl.deleteDatabase(options);

                  case 5:
                    return _context19.abrupt("return", Promise.resolve());

                  case 8:
                    _context19.prev = 8;
                    _context19.t0 = _context19["catch"](2);
                    return _context19.abrupt("return", Promise.reject("".concat(_context19.t0)));

                  case 11:
                    _context19.next = 14;
                    break;

                  case 13:
                    return _context19.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 14:
                    _context19.next = 17;
                    break;

                  case 16:
                    throw this.unimplemented('Not implemented on web.');

                  case 17:
                  case "end":
                    return _context19.stop();
                }
              }
            }, _callee19, this, [[2, 8]]);
          }));

          function deleteDatabase(_x18) {
            return _deleteDatabase.apply(this, arguments);
          }

          return deleteDatabase;
        }()
      }, {
        key: "isJsonValid",
        value: function () {
          var _isJsonValid = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee20(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee20$(_context20) {
              while (1) {
                switch (_context20.prev = _context20.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context20.next = 21;
                      break;
                    }

                    if (this.isStoreOpen) {
                      _context20.next = 5;
                      break;
                    }

                    _context20.next = 4;
                    return this.sqliteEl.isStoreOpen();

                  case 4:
                    this.isStoreOpen = _context20.sent;

                  case 5:
                    if (!this.isStoreOpen) {
                      _context20.next = 18;
                      break;
                    }

                    _context20.prev = 6;
                    _context20.next = 9;
                    return this.sqliteEl.isJsonValid(options);

                  case 9:
                    ret = _context20.sent;
                    return _context20.abrupt("return", Promise.resolve(ret));

                  case 13:
                    _context20.prev = 13;
                    _context20.t0 = _context20["catch"](6);
                    return _context20.abrupt("return", Promise.reject("".concat(_context20.t0)));

                  case 16:
                    _context20.next = 19;
                    break;

                  case 18:
                    return _context20.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 19:
                    _context20.next = 22;
                    break;

                  case 21:
                    throw this.unimplemented('Not implemented on web.');

                  case 22:
                  case "end":
                    return _context20.stop();
                }
              }
            }, _callee20, this, [[6, 13]]);
          }));

          function isJsonValid(_x19) {
            return _isJsonValid.apply(this, arguments);
          }

          return isJsonValid;
        }()
      }, {
        key: "importFromJson",
        value: function () {
          var _importFromJson = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee21(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee21$(_context21) {
              while (1) {
                switch (_context21.prev = _context21.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context21.next = 21;
                      break;
                    }

                    if (this.isStoreOpen) {
                      _context21.next = 5;
                      break;
                    }

                    _context21.next = 4;
                    return this.sqliteEl.isStoreOpen();

                  case 4:
                    this.isStoreOpen = _context21.sent;

                  case 5:
                    if (!this.isStoreOpen) {
                      _context21.next = 18;
                      break;
                    }

                    _context21.prev = 6;
                    _context21.next = 9;
                    return this.sqliteEl.importFromJson(options);

                  case 9:
                    ret = _context21.sent;
                    return _context21.abrupt("return", Promise.resolve(ret));

                  case 13:
                    _context21.prev = 13;
                    _context21.t0 = _context21["catch"](6);
                    return _context21.abrupt("return", Promise.reject("".concat(_context21.t0)));

                  case 16:
                    _context21.next = 19;
                    break;

                  case 18:
                    return _context21.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 19:
                    _context21.next = 22;
                    break;

                  case 21:
                    throw this.unimplemented('Not implemented on web.');

                  case 22:
                  case "end":
                    return _context21.stop();
                }
              }
            }, _callee21, this, [[6, 13]]);
          }));

          function importFromJson(_x20) {
            return _importFromJson.apply(this, arguments);
          }

          return importFromJson;
        }()
      }, {
        key: "exportToJson",
        value: function () {
          var _exportToJson = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee22(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee22$(_context22) {
              while (1) {
                switch (_context22.prev = _context22.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context22.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context22.next = 14;
                      break;
                    }

                    _context22.prev = 2;
                    _context22.next = 5;
                    return this.sqliteEl.exportToJson(options);

                  case 5:
                    ret = _context22.sent;
                    return _context22.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context22.prev = 9;
                    _context22.t0 = _context22["catch"](2);
                    return _context22.abrupt("return", Promise.reject("".concat(_context22.t0)));

                  case 12:
                    _context22.next = 15;
                    break;

                  case 14:
                    return _context22.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context22.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context22.stop();
                }
              }
            }, _callee22, this, [[2, 9]]);
          }));

          function exportToJson(_x21) {
            return _exportToJson.apply(this, arguments);
          }

          return exportToJson;
        }()
      }, {
        key: "createSyncTable",
        value: function () {
          var _createSyncTable = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee23(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee23$(_context23) {
              while (1) {
                switch (_context23.prev = _context23.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context23.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context23.next = 14;
                      break;
                    }

                    _context23.prev = 2;
                    _context23.next = 5;
                    return this.sqliteEl.createSyncTable(options);

                  case 5:
                    ret = _context23.sent;
                    return _context23.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context23.prev = 9;
                    _context23.t0 = _context23["catch"](2);
                    return _context23.abrupt("return", Promise.reject("".concat(_context23.t0)));

                  case 12:
                    _context23.next = 15;
                    break;

                  case 14:
                    return _context23.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context23.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context23.stop();
                }
              }
            }, _callee23, this, [[2, 9]]);
          }));

          function createSyncTable(_x22) {
            return _createSyncTable.apply(this, arguments);
          }

          return createSyncTable;
        }()
      }, {
        key: "setSyncDate",
        value: function () {
          var _setSyncDate = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee24(options) {
            return regeneratorRuntime.wrap(function _callee24$(_context24) {
              while (1) {
                switch (_context24.prev = _context24.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context24.next = 16;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context24.next = 13;
                      break;
                    }

                    _context24.prev = 2;
                    _context24.next = 5;
                    return this.sqliteEl.setSyncDate(options);

                  case 5:
                    return _context24.abrupt("return", Promise.resolve());

                  case 8:
                    _context24.prev = 8;
                    _context24.t0 = _context24["catch"](2);
                    return _context24.abrupt("return", Promise.reject("".concat(_context24.t0)));

                  case 11:
                    _context24.next = 14;
                    break;

                  case 13:
                    return _context24.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 14:
                    _context24.next = 17;
                    break;

                  case 16:
                    throw this.unimplemented('Not implemented on web.');

                  case 17:
                  case "end":
                    return _context24.stop();
                }
              }
            }, _callee24, this, [[2, 8]]);
          }));

          function setSyncDate(_x23) {
            return _setSyncDate.apply(this, arguments);
          }

          return setSyncDate;
        }()
      }, {
        key: "getSyncDate",
        value: function () {
          var _getSyncDate = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee25(options) {
            var ret;
            return regeneratorRuntime.wrap(function _callee25$(_context25) {
              while (1) {
                switch (_context25.prev = _context25.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context25.next = 17;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context25.next = 14;
                      break;
                    }

                    _context25.prev = 2;
                    _context25.next = 5;
                    return this.sqliteEl.getSyncDate(options);

                  case 5:
                    ret = _context25.sent;
                    return _context25.abrupt("return", Promise.resolve(ret));

                  case 9:
                    _context25.prev = 9;
                    _context25.t0 = _context25["catch"](2);
                    return _context25.abrupt("return", Promise.reject("".concat(_context25.t0)));

                  case 12:
                    _context25.next = 15;
                    break;

                  case 14:
                    return _context25.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 15:
                    _context25.next = 18;
                    break;

                  case 17:
                    throw this.unimplemented('Not implemented on web.');

                  case 18:
                  case "end":
                    return _context25.stop();
                }
              }
            }, _callee25, this, [[2, 9]]);
          }));

          function getSyncDate(_x24) {
            return _getSyncDate.apply(this, arguments);
          }

          return getSyncDate;
        }()
      }, {
        key: "addUpgradeStatement",
        value: function () {
          var _addUpgradeStatement = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee26(options) {
            return regeneratorRuntime.wrap(function _callee26$(_context26) {
              while (1) {
                switch (_context26.prev = _context26.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context26.next = 16;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context26.next = 13;
                      break;
                    }

                    _context26.prev = 2;
                    _context26.next = 5;
                    return this.sqliteEl.addUpgradeStatement(options);

                  case 5:
                    return _context26.abrupt("return", Promise.resolve());

                  case 8:
                    _context26.prev = 8;
                    _context26.t0 = _context26["catch"](2);
                    return _context26.abrupt("return", Promise.reject("".concat(_context26.t0)));

                  case 11:
                    _context26.next = 14;
                    break;

                  case 13:
                    return _context26.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 14:
                    _context26.next = 17;
                    break;

                  case 16:
                    throw this.unimplemented('Not implemented on web.');

                  case 17:
                  case "end":
                    return _context26.stop();
                }
              }
            }, _callee26, this, [[2, 8]]);
          }));

          function addUpgradeStatement(_x25) {
            return _addUpgradeStatement.apply(this, arguments);
          }

          return addUpgradeStatement;
        }()
      }, {
        key: "copyFromAssets",
        value: function () {
          var _copyFromAssets = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee27() {
            return regeneratorRuntime.wrap(function _callee27$(_context27) {
              while (1) {
                switch (_context27.prev = _context27.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context27.next = 16;
                      break;
                    }

                    if (!this.isStoreOpen) {
                      _context27.next = 13;
                      break;
                    }

                    _context27.prev = 2;
                    _context27.next = 5;
                    return this.sqliteEl.copyFromAssets();

                  case 5:
                    return _context27.abrupt("return", Promise.resolve());

                  case 8:
                    _context27.prev = 8;
                    _context27.t0 = _context27["catch"](2);
                    return _context27.abrupt("return", Promise.reject("".concat(_context27.t0)));

                  case 11:
                    _context27.next = 14;
                    break;

                  case 13:
                    return _context27.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 14:
                    _context27.next = 17;
                    break;

                  case 16:
                    throw this.unimplemented('Not implemented on web.');

                  case 17:
                  case "end":
                    return _context27.stop();
                }
              }
            }, _callee27, this, [[2, 8]]);
          }));

          function copyFromAssets() {
            return _copyFromAssets.apply(this, arguments);
          }

          return copyFromAssets;
        }()
      }, {
        key: "getDatabaseList",
        value: function () {
          var _getDatabaseList = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee28() {
            var ret;
            return regeneratorRuntime.wrap(function _callee28$(_context28) {
              while (1) {
                switch (_context28.prev = _context28.next) {
                  case 0:
                    if (!(this.sqliteEl != null)) {
                      _context28.next = 21;
                      break;
                    }

                    if (this.isStoreOpen) {
                      _context28.next = 5;
                      break;
                    }

                    _context28.next = 4;
                    return this.sqliteEl.isStoreOpen();

                  case 4:
                    this.isStoreOpen = _context28.sent;

                  case 5:
                    if (!this.isStoreOpen) {
                      _context28.next = 18;
                      break;
                    }

                    _context28.prev = 6;
                    _context28.next = 9;
                    return this.sqliteEl.getDatabaseList();

                  case 9:
                    ret = _context28.sent;
                    return _context28.abrupt("return", Promise.resolve(ret));

                  case 13:
                    _context28.prev = 13;
                    _context28.t0 = _context28["catch"](6);
                    return _context28.abrupt("return", Promise.reject("".concat(_context28.t0)));

                  case 16:
                    _context28.next = 19;
                    break;

                  case 18:
                    return _context28.abrupt("return", Promise.reject("Store \"jeepSqliteStore\" failed to open"));

                  case 19:
                    _context28.next = 22;
                    break;

                  case 21:
                    throw this.unimplemented('Not implemented on web.');

                  case 22:
                  case "end":
                    return _context28.stop();
                }
              }
            }, _callee28, this, [[6, 13]]);
          }));

          function getDatabaseList() {
            return _getDatabaseList.apply(this, arguments);
          }

          return getDatabaseList;
        }()
      }, {
        key: "addSQLiteSuffix",
        value: function () {
          var _addSQLiteSuffix = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee29(options) {
            return regeneratorRuntime.wrap(function _callee29$(_context29) {
              while (1) {
                switch (_context29.prev = _context29.next) {
                  case 0:
                    console.log('addSQLiteSuffix', options);
                    throw this.unimplemented('Not implemented on web.');

                  case 2:
                  case "end":
                    return _context29.stop();
                }
              }
            }, _callee29, this);
          }));

          function addSQLiteSuffix(_x26) {
            return _addSQLiteSuffix.apply(this, arguments);
          }

          return addSQLiteSuffix;
        }()
      }, {
        key: "deleteOldDatabases",
        value: function () {
          var _deleteOldDatabases = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee30(options) {
            return regeneratorRuntime.wrap(function _callee30$(_context30) {
              while (1) {
                switch (_context30.prev = _context30.next) {
                  case 0:
                    console.log('deleteOldDatabases', options);
                    throw this.unimplemented('Not implemented on web.');

                  case 2:
                  case "end":
                    return _context30.stop();
                }
              }
            }, _callee30, this);
          }));

          function deleteOldDatabases(_x27) {
            return _deleteOldDatabases.apply(this, arguments);
          }

          return deleteOldDatabases;
        }()
      }]);

      return CapacitorSQLiteWeb;
    }(_capacitor_core__WEBPACK_IMPORTED_MODULE_0__["WebPlugin"]); //# sourceMappingURL=web.js.map

    /***/

  }
}]);
//# sourceMappingURL=web-es5.js.map