import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.ionicnecco',
  appName: 'Material Control - Loginets',
  webDir: 'www',
  bundledWebRuntime: false,
  cordova: {
    preferences: {
      'android-minSdkVersion': '22',
      'android-targetSdkVersion': '30',
      ScrollEnabled: 'false',
      BackupWebStorage: 'none',
      SplashMaintainAspectRatio: 'true',
      FadeSplashScreenDuration: '300',
      SplashShowOnlyFirstTime: 'true',
      SplashScreen: 'screen',
      SplashScreenDelay: '3000',
      AutoHideSplashScreen: 'false'
    }
  }
};

export default config;
