import { Component } from "@angular/core";
import { ExportService } from "./services/export-service";
import { Platform } from "@ionic/angular";
// import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// import { StatusBar } from '@ionic-native/status-bar/ngx';
import { StatusBar } from "@capacitor/status-bar";
import { MenuService } from "./services/menu-service";
import { NavController } from "@ionic/angular";
import { environment } from "../environments/environment";
import { Router } from "@angular/router";
import { LoginService } from "./services/login-service";
import { CommonService } from "./services/common.service";
import { TranslationService } from "./services/translation-service.service";
import { EventsService } from "./services/events.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { SqliteService } from "./services/sqlite.service";
import { UtilityService } from "./services/utility.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
  providers: [MenuService, ExportService, CommonService],
})
export class AppComponent {
  public appPages = [];
  headerMenuItem = {
    image: "",
    title: "",
    background: "",
  };
  isEnabledRTL: boolean = false;

  constructor(
    private platform: Platform,
    private menuService: MenuService,
    private navController: NavController,
    private router: Router,
    public translationService: TranslationService,
    public events: EventsService,
    public sqlite: SqliteService,
    public utilityService: UtilityService
  ) {
    this.isEnabledRTL = localStorage.getItem("isEnabledRTL") == "true";
    //console.log(JSON.stringify(exportService.export()));

    this.initializeApp();
    this.appPages = this.menuService.getAllThemes();
    this.headerMenuItem = this.menuService.getDataForTheme(null);
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      const flag = await this.sqlite.initialize();
      if (!flag) {
        this.exitApp();
      }
      this.setRTL();
      StatusBar.setOverlaysWebView({ overlay: false });
      StatusBar.setBackgroundColor({ color: "#000000" });
      // this.splashScreen.hide();

      // this.events.subscribe("user:logout", this.logout.bind(this))
      // const flag = await this.sqlite.initializeDatabase();
      // if(!flag){
      //   this.exitApp();
      // }

      if (localStorage.getItem(environment.HAS_LOGGED_IN) === "true") {
        this.router.navigateByUrl("/home");
      } else {
        this.router.navigateByUrl("");
      }
    });
  }
  setRTL() {
    document
      .getElementsByTagName("html")[0]
      .setAttribute("dir", this.isEnabledRTL ? "rtl" : "ltr");
    this.translationService.setDefaultLanguage();
  }

  logout() {
    this.openPage("logout");
  }

  exitApp() {
    navigator["app"].exitApp();
  }

  openPage(page) {
    console.log(page.url, page.url == "stock");
    if (page.url == "stock") {
      this.events.publish("stock:reset");
    }

    if (page.url == "settings") {
      // this.navController.navigateRoot([page.url], {});
      // send an event to dashboard and open modal for settings
      this.events.publish("settings:modal-open");
    } else {
      this.navController.navigateRoot([page.url], {});
    }

    console.log(page.url);
  }
}
