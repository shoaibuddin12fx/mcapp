
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// import { AngularFireModule } from "@angular/fire";
// import { AngularFireDatabaseModule } from "@angular/fire/database";
// import { AngularFireAuthModule } from "@angular/fire/auth";

import { AppSettings } from './services/app-settings';
import { ToastService } from './services/toast-service';
import { LoadingService } from './services/loading-service';

import { Http, HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { IonicStorageModule } from '@ionic/storage';
import { CommonService } from './services/common.service';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslationService } from './services/translation-service.service';
import { StorageService } from './services/storage.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
// import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
// import { SQLite } from '@ionic-native/sqlite/ngx';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { InterceptorService } from './services/interceptor.service';
import { AppErrorHandler } from './AppErrorHandler';
// import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { SqliteService } from './services/sqlite.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterceptorResponseService } from './services/interceptor.response.service';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { SettingPageModule } from './setting/setting.module';
import { SettingPage } from './setting/setting.page';

@NgModule({
  declarations: [AppComponent, SettingPage],
  entryComponents: [],
  imports: [
    // AngularFireModule.initializeApp(AppSettings.FIREBASE_CONFIG),
    // AngularFireDatabaseModule, AngularFireAuthModule,
    BrowserModule, HttpModule, HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    NgxQRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({ 
      loader: {  
        provide: TranslateLoader, 
        useFactory: (createTranslateLoader),  
        deps: [HttpClient] 
      } 
    }), // end translate module
    Ng2SearchPipeModule,
    
  ],
  providers: [
    BarcodeScanner,
    ToastService, LoadingService,CommonService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },    
    { provide: ErrorHandler, useClass: AppErrorHandler },
    NativeStorage,
    TranslationService,
    StorageService,
    // Geolocation,
    AndroidPermissions,
    LocationAccuracy,
    // LaunchNavigator,
    // FirebaseX,
    // SQLite,
    NgxPubSubService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
