import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookContainerPage } from './book-container.page';

describe('BookContainerPage', () => {
  let component: BookContainerPage;
  let fixture: ComponentFixture<BookContainerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookContainerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookContainerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
