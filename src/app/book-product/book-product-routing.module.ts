import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookProductPage } from './book-product.page';

const routes: Routes = [
  {
    path: '',
    component: BookProductPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookProductPageRoutingModule {}
