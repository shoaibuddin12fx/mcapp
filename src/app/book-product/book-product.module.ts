import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookProductPageRoutingModule } from './book-product-routing.module';

import { BookProductPage } from './book-product.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookProductPageRoutingModule,
    ReactiveFormsModule,    
    TranslateModule
  ],
  declarations: [BookProductPage]
})
export class BookProductPageModule {}
