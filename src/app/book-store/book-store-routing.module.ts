import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookStorePage } from './book-store.page';

const routes: Routes = [
  {
    path: '',
    component: BookStorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookStorePageRoutingModule {}
