import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContainerListPage } from './container-list.page';

describe('ContainerListPage', () => {
  let component: ContainerListPage;
  let fixture: ComponentFixture<ContainerListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContainerListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
