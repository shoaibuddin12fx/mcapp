import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {

    let envProject = localStorage.getItem(environment.PROJECT_CODE)
    let envStatuscodes = localStorage.getItem(environment.STATUS_CODE)
    localStorage.clear();
    localStorage.setItem(environment.PROJECT_CODE, envProject)
    localStorage.setItem(environment.STATUS_CODE, envStatuscodes)
    this.router.navigateByUrl('');
  }

}
