import { Component, Injector, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PackageService } from '../services/package.service';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { PackageDetailsPage } from '../package-details/package-details.page';
import { ModalController } from '@ionic/angular';
import { BasePage } from '../pages/base-page/base-page';

@Component({
  selector: 'app-package-list',
  templateUrl: './package-list.page.html',
  styleUrls: ['./package-list.page.scss'],
  providers: [PackageService]
})
export class PackageListPage extends BasePage implements OnInit {

  data: any;
  tempData: any;
  type: string;
  searchControl: FormControl;
  searching: any = false;
  itemClicked: boolean = false;
  tempArray: any = [];
  offset = 0;
  search = '';
  showSpinner = false;
  dataCalledFirstTime;
  constructor(private service: PackageService,
    private commonService: CommonService,
    public router: Router,
    public modalCtrl: ModalController,
     Injector:Injector
    ) {
    super(Injector);
    this.getPackageDetails();
    this.events.subscribe('Packages Service: data called from api',()=>{
      this.dataCalledFirstTime = localStorage.getItem('first_time_calling_data');
      console.log("data fetched event has been called");
    })
  }

  ionViewWillEnter(){
    this.getPackageDetails();
  }

  async doRefresh($event){
    this.showSpinner = true;
    this.offset = 0;
    let obj = await this.service.getPackageList(this.search, this.offset );
    this.offset = obj.offset;
    this.data = this.data;
    if(this.data.length > 0){
      $event.target.complete();
      this.showSpinner = false;
    }
  }

  async loadData($event){
    this.showSpinner = true;
    let obj = await this.service.getPackageList(this.search, this.offset );
    this.offset = obj.offset;
    this.data = [...this.data, ...obj.data];
    $event.target.complete()
    if(this.data.length > 0){
      this.showSpinner = false;
    }

  }

  async getPackageDetails() {
    this.showSpinner = true;
    this.dataCalledFirstTime = localStorage.getItem('first_time_calling_data');
    let obj = await this.service.getPackageList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;
    console.log("problem")
    if(this.data.length > 0){
      this.showSpinner = false;
      
    }
  }

  async addItemClick(params) {
    //console.log('dfdfdf-->'+params);
    //console.log(params);
    
    if(params.isChecked){
        this.tempArray.push(params.id);
    } else {
        this.tempArray.splice(this.tempArray.indexOf(params.id), 1);
    }

    if(this.tempArray.length>0){
        this.itemClicked = true;
    } else {
        this.itemClicked = false;
    }
  }

  async onItemClickFunc(entry) {
    console.log(entry);
    let profileModal = await this.modalCtrl.create(
      {
        component: PackageDetailsPage,
        componentProps:
        {
          "description": entry.description,
          "poNo": entry.poNo,
          "qty": entry.net,
          "containerNo": entry.containerNo,
          "area": entry.area,
          "gross": entry.gross,
          "height": entry.height,
          "length": entry.length,
          "location": entry.location,
          "net": entry.net,
          "packageNo": entry.packageNo,
          "supplierText": entry.supplierText,
          "volume": entry.volume,
          "width": entry.width
        }
      }
    );
    return profileModal.present();
    //}
  }
  ngOnInit() {
  }

  async onSearchTerm($event) {

    console.log($event.target.value);
    this.search = $event.target.value;
    this.offset = 0;
    let obj = await this.service.getPackageList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;

    // let searchVal = searchTerm.detail.value;
    // this.data = this.tempData;

    // if (searchVal && searchVal.trim() !== '') {

    //   this.data = this.data.filter(item => {
    //     return item.packageNo.indexOf(searchVal) > -1;
    //   });
    // }
  }
  
  bookNow(){
    let checkedData = this.data.filter( x => x.isChecked == true )
    this.commonService.addContainerData(checkedData);
    this.router.navigateByUrl("book-package");
  }
}


