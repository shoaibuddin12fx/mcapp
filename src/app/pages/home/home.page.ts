import { AppSettings } from './../../services/app-settings';
import { Component, OnInit } from '@angular/core';
import { HomeService } from './../../services/home-service';
import { ModalController } from '@ionic/angular';
import { IntroPage } from '../intro-page/intro-page.page';
import { EventsService } from 'src/app/services/events.service';
import { SettingPage } from 'src/app/setting/setting.page';
import { PackageService } from 'src/app/services/package.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [HomeService]
})
export class HomePage implements OnInit{

  item;
  offset = 0;
  search = '';
  dataLoaded = false;
  constructor(
    private homeService:HomeService, 
    private service: PackageService,
    public modalController: ModalController,
    public events: EventsService
    ) { 
      this.item = this.homeService.getData();
      let showWizard = localStorage.getItem("SHOW_START_WIZARD");

      if (AppSettings.SHOW_START_WIZARD && !showWizard) {
        this.openModal()
      }

      console.log("token refresh");
      homeService.tokenRefresh()
      console.log("home  page loaded")
      this.getPackageData();
  }

  ngOnInit(){
    this.events.subscribe("settings:modal-open",this.openSettingsModal.bind(this))
  }

  async getPackageData(){
    let list = await this.service.getPackageList();

    if(list){
      this.dataLoaded = true;
      console.log("data loaded: ",this.dataLoaded);
    }
  }

  async openModal() {
    let modal = await this.modalController.create({component: IntroPage});
     return await modal.present();
  }

  async openSettingsModal(){
    
    let modal = await this.modalController.create({component:SettingPage})
    return await modal.present();
  }  


}
