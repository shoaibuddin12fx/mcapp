import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { SharedModule } from './../../components/shared.module';

import { ItemDetailsCheckBoxPage } from './item-details-check-box.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: ItemDetailsCheckBoxPage
      }
    ]),
    TranslateModule
  ],
  declarations: [ItemDetailsCheckBoxPage],
  exports:[ItemDetailsCheckBoxPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ItemDetailsCheckBoxPageModule {}
