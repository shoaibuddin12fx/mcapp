import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { LoginService } from '../../services/login-service';
import { ToastService } from 'src/app/services/toast-service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Component({
    templateUrl: 'item-details-login.page.html',
    styleUrls: ['item-details-login.page.scss'],
    providers: [LoginService]

})
export class ItemDetailsLoginPage {

    data = {};
    type: string;
    loginFailure = true;
    loading = false;
    private isLoggedIn = new BehaviorSubject<boolean>(false);
    public isLoggedIn$ = this.isLoggedIn.asObservable();
   
    constructor(
        public navCtrl: NavController,
        private service: LoginService,
        private toastCtrl: ToastService,
        private route: ActivatedRoute,
        private router: Router) {
        this.type = this.route.snapshot.paramMap.get('type');
        this.service.load(service.getAllThemes()[this.type]).subscribe(d => {
            this.data = d;
        });
    }

    isType(item) {
        return item === parseInt(this.type, 10);
    }

    // events
    onLogin(params): void {
        localStorage.setItem('login_email_password', JSON.stringify(params) )
        this.loading = true;
        this.service.login(params).subscribe(data =>{
            this.loading = false;
            console.log(data);
            if(data.body.code == 200){
                console.log('Login');
                localStorage.setItem(environment.HAS_LOGGED_IN,"true");
                localStorage.setItem(environment.TOKEN, data.body.responseData.token);
                localStorage.setItem(environment.REFRESH_TOKEN, data.body.responseData.refreshToken);                
                localStorage.setItem(environment.USER_NAME, data.body.responseData.username);
                localStorage.setItem(environment.ACCOUNT_ID, data.body.responseData.accountId);
                localStorage.setItem(environment.IS_SUPERVISOR, data.body.responseData.isSupervisor);
                
                
                this.router.navigateByUrl('/home');
                this.isLoggedIn.next(true);
            } else {
                this.toastCtrl.presentToast('Login Failure');
                console.log('Login Failure');
                this.loginFailure = false;
                this.router.navigateByUrl('');
            }
            
        },error =>{
            this.toastCtrl.presentToast('Login Failure');
            console.log('Login Failure');
            this.loginFailure = false;
            this.router.navigateByUrl('');
        });
    }
    onRegister(params): void {
        this.toastCtrl.presentToast('onRegister:' + JSON.stringify(params));
    }
    onSkip(): void {
        this.toastCtrl.presentToast('onSkip');
    }
    onFacebook(params): void {
        this.toastCtrl.presentToast('onFacebook:' + JSON.stringify(params));
    }
    onTwitter(params): void {
        this.toastCtrl.presentToast('onTwitter:' + JSON.stringify(params));
    }
    onGoogle(params): void {
        this.toastCtrl.presentToast('onGoogle:' + JSON.stringify(params));
    }
    onPinterest(params): void {
        this.toastCtrl.presentToast('onPinterest:' + JSON.stringify(params));
    }
}
