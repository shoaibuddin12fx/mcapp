import { Component, OnInit } from '@angular/core';
import { BarcodeScannerOptions, BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { QRCodeService } from 'src/app/services/qrcode-service';
import { AlertController, LoadingController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading-service';
import { CommonService } from 'src/app/services/common.service';
import { BobjectsService } from 'src/app/services/bobjects.service';

@Component({
  selector: 'app-readqrcode',
  templateUrl: './readqrcode.page.html',
  styleUrls: ['./readqrcode.page.scss'],
  providers: [QRCodeService]
})
export class ReadqrcodePage implements OnInit {

  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  title = 'app';
  elementType = 'url';
  value = 'FWE4090';
  createdCode: any;
  qrCodeMap = new Map();
  showMainBtn: boolean = true;
  scannedDataValue = new Set();
  tempArray: any = [];
  loading: any;
  noFound: boolean = false;

  date;
  hours:any;
  minutes:any;
  seconds:any;
  currentLocale: any;
  timer;
  isTwelveHrFormat:false;

  constructor(private barcodeScanner: BarcodeScanner,
    public bObject: BobjectsService,
    private qrCodeService: QRCodeService,
    private alertCtrl: AlertController,
    private router: Router,
    public platform: Platform,
    private commonService: CommonService) {
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }
  
  ionViewDidEnter(){
    console.log('inside load');
    this.initialize()
  }

  ngOnInit(): void {
    console.log('inside init');
  }

  async initialize(){
    var bObj = await this.bObject.prepareObject();    
    console.log(bObj)
    this.scanCode();
    
  }


  async scanCode() {

    clearInterval(this.timer);
    this.callTimer();
    if(this.platform.is('cordova')){
      this.barcodeScanner
      .scan()
      .then(async barcodeData => {
        this.scannedData = barcodeData.text;
       
        var bObj = await this.bObject.prepareObject()
        bObj['qrCodeValue'] = barcodeData.text;
      
        await this.validateQRCode(bObj);
      })
      .catch(err => {
        console.log("Error", err);
      });
    }else{
      // let array = ["180377290", "GLDU070153-1", "TAU0009"]
      let array = ["33627"]
      const randomElement = array[Math.floor(Math.random() * array.length)];
      this.scannedData = randomElement;
      var bObj = await this.bObject.prepareObject();
      console.log(bObj)
      bObj['qrCodeValue'] = randomElement;
      await this.validateQRCode(bObj);

    }

    
  }

  async validateQRCode(scannedDataTmp) {
    this.scannedData = scannedDataTmp;
    this.scannedDataValue.add(scannedDataTmp);
    this.showMainBtn = false;
    
    console.log()
    this.qrCodeService.validateQrCode(scannedDataTmp).subscribe(res => {
      console.log(res);
      if (res.responseData.type != 0) {
        localStorage.setItem("qrcode_orderLine",JSON.stringify(res.responseData))
        localStorage.setItem("qrcode_id", res.responseData.id )
        localStorage.setItem("qrcode_type", res.responseData.type )
        localStorage.setItem("qrcode_code", JSON.stringify(scannedDataTmp.qrCodeValue) )
        localStorage.setItem("qrcode_description", res.responseData.description )
        this.qrCodeMap.set(res.responseData.id, res.responseData.type);
        this.bookNow();
      } else{
        this.noFound = true;
      }
    }, error => {
      //this.loading.dismissAll();
    });
  }

  ionViewDidLeave(){
    console.log('inside destroy');
    this.scannedData = "";
    this.scannedDataValue = new Set();
    this.showMainBtn = true;
  }

  createCode() {
    this.scannedData = this.value;
  }

  async bookNow() {
    for (let [key, value] of this.qrCodeMap) {
      
      if (value == 4) {
        this.tempArray.splice(0, this.tempArray.length)
        this.tempArray.push(key);
        
        this.commonService.addContainerData(this.tempArray);
        this.commonService.addFromQRCode([true]);
        this.router.navigateByUrl("book-product");
      
      } else if (value == 2) {
        this.tempArray.splice(0, this.tempArray.length)
        this.tempArray.push(key);
        
        this.commonService.addContainerData(this.tempArray);
        this.commonService.addFromQRCode([true]);
        this.router.navigateByUrl("book-container");

      } else if (value == 1) {
        this.tempArray.splice(0, this.tempArray.length)
        this.tempArray.push[key];
        
        this.commonService.addPackageData(this.tempArray);
        this.commonService.addFromQRCode([true]);
        this.router.navigateByUrl("book-package");

      } else if (value == 3) {
        this.tempArray.splice(0, this.tempArray.length)
        this.tempArray.push[key];

        this.commonService.addShipmentData(this.tempArray);
        this.commonService.addFromQRCode([true]);
        this.router.navigateByUrl("book-store");
        
      }

    }
  }

  callTimer(){
    this.timer = setInterval(() =>{
      const currentDate = new Date();
      this.date = currentDate.toLocaleTimeString();
    }, 1000);
  }

}
