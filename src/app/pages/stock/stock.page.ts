import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StockService } from 'src/app/services/stock.service';
import { TranslationService } from 'src/app/services/translation-service.service';
import { UtilityService } from 'src/app/services/utility.service';
import { environment } from 'src/environments/environment';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.page.html',
  styleUrls: ['./stock.page.scss'],
})
export class StockPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitted = false;
  showSearch = true;
  data = [];
  filterTerm;
  offset = 0;
  search = '';
  showSpinner = false;
  page;

  constructor(injector: Injector, public router: Router, public stockService: StockService, public utility: UtilityService, public translation: TranslationService) { 
    super(injector);
    this.setupForm();
    if(!this.showSearch){
      this.getStockDetails();
    }

   
  }

  resetSearch(){
    console.log("ping")
    this.showSearch = true;
    this.aForm.reset();
  }

  

  ionViewWillEnter(){
    // if(this.router.url == 'stock'){
    //   this.showSearch = true
    //   console.log(this.showSearch);
    // }

  }

  ngOnInit() {
    this.events.subscribe("stock:reset", this.resetSearch.bind(this))
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      areaNo: [''],
      posNo: [''],
      description: ['' ],
      productCode: ['' ],
      supplier: [''],
      poNo: [''],
      drawingNo: [''],

      
    });
  }

  async getStockDetails(){
    this.showSpinner = true;
    let obj = await this.stockService.getStockList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;
    console.log("problem")
    this.showSpinner = false;
  }

  async searchForm(){
    
//     {"username":"ProjectOffice","token":"abcd", 
// "ownerId":1, 
// "texts":[],
// "posNo":"",
// "drawingNo":"703078764",
// "description":"",
// "productCode":"",	
// "poNo":""
// }

    let formdata = this.aForm.value;

    formdata['ownerId'] = localStorage.getItem(environment.PROJECT_CODE);

    if(!formdata['ownerId']){

      this.translation.getTranslateKey("project_selection_error").then( v => {
        this.utility.presentFailureToast(v)
      })

      this.router.navigateByUrl('setting')
      
      return 
    }
    
    if(
      (formdata['areaNo'] == '' || formdata['areaNo'] == null)
      && (formdata['posNo'] == '' || formdata['posNo'] == null)
      && (formdata['description'] == '' || formdata['description'] == null)
      && (formdata['productCode'] == '' || formdata['productCode'] == null)
      && (formdata['supplier'] == '' || formdata['supplier'] == null)
      && (formdata['poNo'] == '' || formdata['poNo'] == null)
      && (formdata['drawingNo'] == '' || formdata['drawingNo'] == null)
    ){

      this.utility.presentFailureToast("At least one field is required to search");
      return;

    }

    formdata['texts'] = formdata['areaNo'] != "" ? [formdata['areaNo']] : [];
    delete formdata['areaNo'];




    this.submitted = true;
    this.showSpinner = true;
    // this.utility.showLoader();
    this.showSearch = false;
    console.log(formdata)
    
    setTimeout(async ()=>{
      var obj:any = await this.stockService.getStockListByUrl(formdata);
      // this.utility.hideLoader();
      this.showSpinner = false;
      this.offset = obj.offset;
      this.data = obj.data;
    },1000)

  }


  async loadData($event){
    if(!this.showSearch){
    let obj = await this.stockService.getStockList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = [...this.data, ...obj.data];
    $event.target.complete()
    }
  }



  delayTimer;
  async onSearchTerm($event) {
    console.log("Working")
    let v = $event.target.value;
    clearTimeout(this.delayTimer);
    

      this.delayTimer = setTimeout( async () => {
        this.offset = 0;
        let obj = await this.stockService.getStockList(v, this.offset);
        console.log(obj)
        this.offset = obj.offset;
        this.data = obj.data;
      }, 500)

    

    
    
    
    // let searchVal = searchTerm.detail.value;
    // this.data = this.tempData;

    // if (searchVal && searchVal.trim() !== '') {

    //   this.data = this.data.filter(item => {
    //     return item.containerNo.indexOf(searchVal) > -1;
    //   });
    // }
  }

  
}
