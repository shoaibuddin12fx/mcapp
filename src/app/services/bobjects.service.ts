import { Injectable } from '@angular/core';
import { GeolocationsService } from './geolocations.service';
import { StorageService } from './storage.service';
import { TranslationService } from './translation-service.service';
import { UserService } from './user.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class BobjectsService {

  constructor(
    public users: UserService, 
    public geo: GeolocationsService, 
    public translation: TranslationService,
    public storage: StorageService) { 

      
    }

    prepareObject(): Promise<any>{

      return new Promise( async resolve => {

        await this.geo.checkLocations();
        const coords = await this.geo.getLocationCoordinates();
        const locale = await this.translation.getLocale();
        // const token = await this.users.getUserToken();
        // const user = await this.users.getUserData();
        console.log(coords);
  
        var obj = {
          // "username": user ? user['username'] : null,
          // "id": user ? user['userId'] : null,
          // "token": token,
          "locale": locale,
          "timestamp": Math.floor(Date.now()/1000),          
          "isMobile":true
        };

        obj = {...obj, ...coords}
        resolve(obj);
      })
  
    }
}
