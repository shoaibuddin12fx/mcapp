import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookstoreService {

  constructor(private http:HttpClient) { }

  insertShipment(comment, arr:[]) : Observable<any> {
    var url = environment.SERVER_URL+"/rest/shipments/confirm";
   
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    });

    let request = {
      'username': localStorage.getItem(environment.USER_NAME),
      'token': localStorage.getItem(environment.TOKEN),
      'locale': 'fi',
      'ids':arr,
      'confirmDate' :new Date().getTime(),
      'location':`${comment.value.area}`,
      'comment': comment.value.name,      
      }
    return this.http.post(url,request, {headers: headers});

  }

  insertContainer(comment, arr:[]) : Observable<any> {
    var url = environment.SERVER_URL+"/rest/containers/confirm";
   
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    });

    let request = {
      'username': localStorage.getItem(environment.USER_NAME),
      'token': localStorage.getItem(environment.TOKEN),
      'locale': 'fi',
      'ids':arr,
      'confirmDate' :new Date().getTime(),
      'location':`${comment.value.area}`,
      'comment': comment.value.name,      
      }
    return this.http.post(url,request, {headers: headers});

  }

  insertPackages(comment, arr:[]) : Observable<any> {
    var url = environment.SERVER_URL+"/rest/packages/confirm";
   
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    });

    let request = {
      'username': localStorage.getItem(environment.USER_NAME),
      'token': localStorage.getItem(environment.TOKEN),
      'locale': 'fi',
      'ids':arr,
      'confirmDate' :new Date().getTime(),
      'location':`${comment.value.area}`,
      'comment': comment.value.name,      
      }
    return this.http.post(url,request, {headers: headers});

  }

  insertStockitem(comment, arr:[]) : Observable<any> {
    var url = environment.SERVER_URL+"/rest/stockitems/confirm";
   
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    });

    let request = {
      'username': localStorage.getItem(environment.USER_NAME),
      'token': localStorage.getItem(environment.TOKEN),
      'locale': 'fi',
      'ids':arr,
      'confirmDate' :new Date().getTime(),
      'location':`${comment.value.area}`,
      'comment': comment.value.name,      
      }
    return this.http.post(url,request, {headers: headers});

  }

}
