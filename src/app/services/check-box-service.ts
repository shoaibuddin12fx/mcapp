import { IService } from './IService';
// import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
import { LoadingService } from './loading-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TranslationService } from './translation-service.service';
import { UtilityService } from './utility.service';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class CheckBoxService implements IService {

  constructor(
    private loadingService: LoadingService,
    public translation: TranslationService,
    public utility: UtilityService,
    public router: Router,
    private http:HttpClient) { }

  getShippingDetails(): Observable<any> {
    var url = environment.SERVER_URL+"/rest/shipments/list";
   
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    });

    let projectId = localStorage.getItem(environment.PROJECT_CODE);

    if(!projectId){

      this.translation.getTranslateKey("project_selection_error").then( v => {
        this.utility.presentFailureToast(v)
      })
      
      this.router.navigateByUrl('setting')

      return 
    }

    let request = {
      'username': localStorage.getItem(environment.USER_NAME),
      'token': localStorage.getItem(environment.TOKEN),
      'locale': 'en_US',
      'projectId': localStorage.getItem(environment.PROJECT_CODE),
      'statusCodes': localStorage.getItem(environment.STATUS_CODE).split(',')
      }
    return this.http.post(url,request, {headers: headers});
  }

  getTitle = (): string => 'Check Boxes';


  getAllThemes = (): Array<any> => {
    return [
      { 'url': 'check-boxes/0', 'title': 'Simple', 'theme': 'layout1' },
      { 'url': 'check-boxes/1', 'title': 'With Avatar', 'theme': 'layout2' },
      { 'url': 'check-boxes/2', 'title': 'Shipment Info', 'theme': 'layout3' }
    ];
  }

  getDataForTheme = (menuItem: any): any => {
    return this[
      'getDataFor' +
      menuItem.theme.charAt(0).toUpperCase() +
      menuItem.theme.slice(1)
    ]();
  }

  //* Data Set for page 1
  getDataForLayout1 = (): any => {
    return {
      'toolbarTitle': 'Simple',
      "items": [
        {
          "id": 1,
          "title": "marshall@yahoo.com",
          "isChecked": false
        },
        {
          "id": 2,
          "title": "cain@outlook.com",
          "isChecked": false
        },
        {
          "id": 4,
          "title": "meadows@mail.com",
          "isChecked": true
        },
        {
          "id": 3,
          "title": "cain@outlook.com",
          "isChecked": true
        },
        {
          "id": 5,
          "title": "meadows@mail.com",
          "isChecked": true
        },
        {
          "id": 6,
          "title": "valdez@yahoo.com",
          "isChecked": true
        },
        {
          "id": 7,
          "title": "norris@gmail.com",
          "isChecked": true
        },
        {
          "id": 8,
          "title": "wiley@outlook.com",
          "isChecked": true
        },
        {
          "id": 9,
          "title": "norris@gmail.com",
          "isChecked": true
        },
        {
          "id": 10,
          "title": "wiley@outlook.com",
          "isChecked": true
        },
        {
          "id": 11,
          "title": "miles@mail.com",
          "isChecked": true
        },
        {
          "id": 12,
          "title": "gaines@yahoo.com",
          "isChecked": true
        },
        {
          "id": 13,
          "title": "fletcher@outlook.com",
          "isChecked": true
        }
      ]
    }
  }

  //* Data Set for page 2
  getDataForLayout2 = (): any => {
    return {
      'toolbarTitle': 'With Avatar',
      "items": [
        {
          "id": 1,
          "title": "Grant Marshall",
          "subtitle": "@grant333",
          "isChecked": false,
          "image": "assets/imgs/avatar/1.jpg"
        },
        {
          "id": 2,
          "title": "Pena Valdez",
          "subtitle": "@penaxxy",
          "isChecked": false,
          "image": "assets/imgs/avatar/2.jpg"
        },
        {
          "id": 4,
          "title": "Jessica Miles",
          "subtitle": "@jessica957",
          "isChecked": true,
          "image": "assets/imgs/avatar/3.jpg"
        },
        {
          "id": 3,
          "title": "Kerri Barber",
          "subtitle": "@kerri333",
          "isChecked": true,
          "image": "assets/imgs/avatar/4.jpg"
        },
        {
          "id": 5,
          "title": "Natasha Gamble",
          "subtitle": "@natashaxxy",
          "isChecked": true,
          "image": "assets/imgs/avatar/5.jpg"
        },
        {
          "id": 6,
          "title": "White Castaneda",
          "subtitle": "@white34",
          "isChecked": true,
          "image": "assets/imgs/avatar/6.jpg"
        },
        {
          "id": 7,
          "title": "Vanessa Ryan",
          "subtitle": "@vanessa957",
          "isChecked": true,
          "image": "assets/imgs/avatar/7.jpg"
        },
        {
          "id": 8,
          "title": "Meredith Hendricks",
          "subtitle": "@meredith957",
          "isChecked": true,
          "image": "assets/imgs/avatar/1.jpg"
        },
        {
          "id": 9,
          "title": "Carol Kelly",
          "subtitle": "@carolm_e",
          "isChecked": true,
          "image": "assets/imgs/avatar/2.jpg"
        },
        {
          "id": 10,
          "title": "Barrera Ramsey",
          "subtitle": "@barreraxxy",
          "isChecked": true,
          "image": "assets/imgs/avatar/3.jpg"
        }
      ]
    }
  }

  //* Data Set for page 3
  getDataForLayout3 = (): any => {
    return {
      'toolbarTitle': 'Shipment Info',
      "items": [
        {
          "id": 1,
          "title": "FWE4022",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": true,
          "detail": "2020-06-09"
        },
        {
          "id": 2,
          "title": "FWE4023",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": true,
          "detail": "2020-06-10"
        },
        {
          "id": 3,
          "title": "FWE4024",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": true,
          "detail": "2020-06-10"
        },
        {
          "id": 4,
          "title": "FWE4025",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": true,
          "detail": "2020-06-10"
        },
        {
          "id": 5,
          "title": "FWE4026",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": true,
          "detail": "2020-06-10"
        },
        {
          "id": 6,
          "title": "FWE4027",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": false,
          "detail": "2020-06-10"
        },
        {
          "id": 7,
          "title": "FWE4028",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": false,
          "detail": "2020-06-10"
        },
        {
          "id": 8,
          "title": "FWE4029",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": false,
          "detail": "2020-06-10"
        },
        {
          "id": 9,
          "title": "FWE4030",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": false,
          "detail": "2020-06-10"
        },
        {
          "id": 10,
          "title": "FWE4031",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": false,
          "detail": "2020-06-10"
        },
        {
          "id": 11,
          "title": "FWE4031",
          "subtitle": "100, 101, 102, 103, 199",
          "isChecked": false,
          "detail": "2020-06-10"
        }
      ]
    }
  }

  load(item: any): Observable<any> {
    const that = this;
      return new Observable(observer => {
        observer.next(this.getDataForTheme(item));
        observer.complete();
      });
    }
}
