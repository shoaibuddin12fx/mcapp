import { Injectable } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
// import { Geolocation } from '@capacitor/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Platform } from '@ionic/angular';
import {Capacitor} from '@capacitor/core';
declare var google;

@Injectable({
  providedIn: 'root'
})
export class GeolocationsService {


  locationCoords: any;
  timetest: any;

  constructor(
    public androidPermissions: AndroidPermissions, 
    public platform: Platform,
    public locationAccuracy: LocationAccuracy) { 

    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    }
    this.timetest = Date.now();

  }

  async checkLocations(){

    return new Promise( async resolve => {

      function timeout(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }
  
      var permissions = Capacitor.isNativePlatform() ? this.checkPermissions() : this.checkGeolocation();
      
      var [args] = await Promise.all([
        permissions,
        timeout(500)
      ]);
        
      resolve(args);

    })
    
    
  }

  checkGeolocation():Promise<any> {

    return new Promise(async resolve => {

      const coords = await this.getCurrentLocationCoordinates();
      // console.log(coords);
      resolve(coords)
    })
    
  }

  checkPermissions(){
    return new Promise((resolve) => {
      resolve(this.checkGPSPermission());
    })
  }

  getCoordsForGeoAddress(address, _default = true) {

    var self = this;
    return new Promise(resolve => {
      var self = this;
      var geocoder = new google.maps.Geocoder;
      geocoder.geocode({ 'address': address }, function (results, status) {
        if (status === 'OK') {
          if (results[0]) {


            var loc = results[0].geometry.location
            var lat = loc.lat();
            var lng = loc.lng();
            resolve({ lat: lat, lng: lng })

          } else {
            resolve(null);
          }
        } else {
          console.log({results, status})
          resolve(null)
        }
      });
    })

  }

  getCoordsViaHTML5Navigator() {

    return new Promise((resolve) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          resolve(pos)

        }, function () {
          resolve({ lat: 51.5074, lng: 0.1278 });
        });
      } else {
        // Browser doesn't support Geolocation
        resolve({ lat: 51.5074, lng: 0.1278 });
      }
    })


  }

  getCurrentLocationCoordinates(){
    return new Promise( async resolve => {
      // Geolocation.getCurrentPosition().then( coords => {
        
      //   var lt = coords.coords.latitude
      //   var lg = coords.coords.longitude
      //   resolve({lat: lt, lng: lg});
      // }, async err => {
        // console.log(err);
        let coords = await this.getCoordsViaHTML5Navigator();
        resolve(coords);
      // });

      
      
    })

  }

  checkGPSPermission(): Promise<any> {
    return new Promise( resolve => {
  
            //If having permission show 'Turn On GPS' dialogue
            this.askToTurnOnGPS().then( v => resolve(v));
    })
    
  }

  requestGPSPermission(): Promise<any> {

    return new Promise(async resolve => {
          //Show 'GPS Permission Request' dialogue
          
                // call method to turn on GPS
                let coords = await this.getCoordsViaHTML5Navigator();
                resolve(coords);                
              
    })
    
  }

  askToTurnOnGPS(): Promise<any> {
    return new Promise((resolve) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        async () => {
          // When GPS Turned ON call method to get Accurate location coordinates
          const coords = await this.getLocationCoordinates()
          resolve(coords)
        }, async error => {
          // alert('Error requesting location permissions ' + JSON.stringify(error))
          let coords = await this.getCoordsViaHTML5Navigator();
          resolve(coords);
          
        });
    })
  }

  getLocationCoordinates(): Promise<any> {
    return new Promise(async resolve => {
      // Geolocation.getCurrentPosition().then((resp) => {
      //   this.locationCoords.latitude = resp.coords.latitude;
      //   this.locationCoords.longitude = resp.coords.longitude;
      //   this.locationCoords.accuracy = resp.coords.accuracy;
      //   this.locationCoords.timestamp = resp.timestamp;
      //   resolve({lat: this.locationCoords.latitude , lng: this.locationCoords.longitude });
      //   resolve(this.locationCoords);
      // }).catch(async (error) => {
      //   alert('Error getting location' + error);
        let coords = await this.getCoordsViaHTML5Navigator();
        resolve(coords);
      // });
    })
    
  }

  

}
