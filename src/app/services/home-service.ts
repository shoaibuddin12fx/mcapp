import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class HomeService {

    constructor(public http:HttpClient) { }

    // Set data for - HOME PAGE
    getData = () => {
        return {
            // 'toolbarTitle': 'Home',
            'title': 'Material Handling System',
            'subtitle1': 'Software as SAAS',
            'subtitle2': 'Versatile but easy to use',
            'subtitle3': 'Automated shipments reception',
            'subtitle4': 'Scalable',
            'link': '',
            'description': 'The Material Control system is an easy to use solution for managing deliveries. It meets international standards and its usability and functionalities have been developed with the newest technologies.',
            'background': 'assets/imgs/background/22.jpg'
        };
    }

    load(): Observable<any> {
            return new Observable(observer => {
                observer.next(this.getData());
                observer.complete();
            });
    }

    async tokenRefresh(){


        setInterval( async () => {
    
          var url = environment.SERVER_URL+"/rest/login/refresh";
       
          console.log("boo")
          var headers = new HttpHeaders({
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
          });
    
          let request = {
            'username': localStorage.getItem(environment.USER_NAME),
            'token': localStorage.getItem(environment.TOKEN),
            'refreshToken': localStorage.getItem(environment.REFRESH_TOKEN),
            'locale': 'fi',        
            }
          this.http.post(url,request, {headers: headers}).subscribe( v => {
            console.log(v, v['responseData']['token'], v['responseData']['refreshToken']);
            localStorage.setItem( environment.TOKEN, v['responseData']['token'] );
            localStorage.setItem( environment.REFRESH_TOKEN, v['responseData']['refreshToken'] );
          });
    
          
    
        }, 2700000)
    
        
    
        
      }
}
