import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
import { Observable, from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { StorageService } from './storage.service';
import { EncryptService } from '../encrypt.service';
import { BobjectsService } from './bobjects.service';
import { EventsService } from './events.service';


@Injectable({
  providedIn: 'root' 
})
export class InterceptorService implements HttpInterceptor  {

  constructor(private storage: StorageService, public utility: UtilityService, public encrypt: EncryptService, public bObject: BobjectsService, public events: EventsService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.getCoords()).pipe(
      switchMap(data => {
        const cloneRequest = this.addParams(req, data );
        return next.handle(cloneRequest)
        .pipe(map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                event = event.clone({body: this.modifyBody(event.body)});
            }
            return event;
        }));
      })
    );

    
    
  }

  modifyBody(body){
    
    let code = body.code;
    if(code && code == 401){
      this.events.publish("user:logout")
    }
    
    return body
  }

  getCoords(){
    return new Promise( async resolve => {
      let obj = await this.bObject.prepareObject();
      console.log(obj)
      resolve(obj);
    })
      
  }

  private addParams(request: HttpRequest<any>, data = {}){

    var bobj = request.body;
    if(bobj){
      bobj = {...bobj, ...data}
      console.log(bobj)      
    }

    let clone = request.clone({ body: bobj })
    return clone;
  }


}
