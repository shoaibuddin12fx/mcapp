import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
import { LoadingService } from './loading-service';

@Injectable({ providedIn: 'root' })
export class IntroService {

  constructor( private loadingService: LoadingService) { }

  // Set data for - WIZARD MAIN
  getData = (): any => {
    return {
      'btnPrev': 'Previous',
      'btnNext': 'Next',
      'btnFinish': 'Finish',
      "items": [
        {
          "logo": "assets/imgs/logo/2.png",
          "title": "Welcome to our new iOS style theme",
          "description": "Finished layouts and components for Ionic . Ready to use!"
        },
        {
          "logo": "assets/imgs/logo/2.png",
          "title": "For Developers",
          "description": "Save hours of developing. Tons of funcional components."
        },
        {
          "logo": "assets/imgs/logo/2.png",
          "title": "For Designers",
          "description": "Endless possibilities. Combine layouts as you wish."
        }
      ]
    };
  }

  load(): Observable<any> {
    const that = this;
      return new Observable(observer => {
        observer.next(this.getData());
        observer.complete();
      });
    }
}
