import { LoadingController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class LoadingService {
  loading: any;
  constructor(private loadingCtrl: LoadingController) { }

  async showLoader(msg = "") {
    this.loading = await this.loadingCtrl.create(
     { 
       message: msg,
     });
    this.loading.present();
  }

  async hideLoader(msg = "") {
    // if(this.loading){
      await this.loading.dismiss();
    // }
   
  }

  async show() {
    //this.loading = await this.loadingCtrl.create(
   //   { 
   //     message: 'Please wait...',
   //   });
   // this.loading.present();
  }

  async hide() {
   // await this.loading.dismiss();
  }
}
