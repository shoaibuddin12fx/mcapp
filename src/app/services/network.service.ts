import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { EventsService } from './events.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {


  login(data) {
    return this.httpPostResponse('login', data, null, true)
  }

  getUser(data, loader = false, error = false) {
    return this.httpPostResponse('users', data, null, loader, error)
  }

  getItsQrcode(data){
    return this.httpPostResponse('tasks/qrcode', data, null, true)
  }

  getItsQrcodeInOut(data){
    return this.httpPostResponse('tasks/inOut', data, null, true)
  }

  getAllProjects(data){
    return this.httpPostResponse('projects/list', data, null, true)
  }

  getAllLitteras(data){
    return this.httpPostResponse('litteras', data, null, true)
  }

  getAllWorks(data){
    return this.httpPostResponse('orders/list', data, null, true)
  }

  constructor(
    public utility: UtilityService,
    public api: ApiService,
    private events: EventsService,
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
  }

  httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
  }

  // default 'Content-Type': 'application/json',
  httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json'): Promise<any> {

    return new Promise( ( resolve, reject ) => {

      console.log(key);
      if (showloader == true) {
        this.utility.showLoader();
      }

      const _id = (id) ? '/' + id : '';
      const url = key + _id;

      console.info(url);
      const seq = (type == 'get') ? this.api.get(url, {}) : this.api.post(url, data);

      seq.subscribe((data: any) => {

        console.log(data);
        let res = data['responseData']

        if (showloader == true) {
          this.utility.hideLoader();
        }

        console.log(data['code'])
        if (data['code'] != 200) {
          if (showError) {
            const myVar = data['responseData'];
            if (typeof myVar === 'string' || myVar instanceof String){
              this.utility.presentSuccessToast(data['responseData']);
            }else{
              if (!data['responseData']){
                this.utility.presentSuccessToast("Request Failed");
                reject(null);
                return;
              }
              if(!data['responseData']['feedback']){
                this.utility.presentSuccessToast("Request Failed");
                reject(null);
                return;
              }

              let failures = data['responseData']['feedback']['failures'];
              if(failures.length > 0){
                let msg = failures[0]['name'];
                if(msg){
                  this.utility.presentSuccessToast(msg);
                }else{
                  this.utility.presentSuccessToast("Request Failed");
                }
                
              }else{
                this.utility.presentSuccessToast("Request Failed");
              }

              
            }

            
          }
          reject(null);
        } else {
          resolve(res);
        }

      }, err => {

        let error = err;
        if (showloader == true) {
          this.utility.hideLoader();
        }

        if (showError) {
          this.utility.presentFailureToast(error['responseData']);
        }

        console.log(err);

        reject(null);

      });

    });

  }

}
