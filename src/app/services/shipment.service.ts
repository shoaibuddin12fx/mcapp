import { IService } from './IService';
// import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
import { LoadingService } from './loading-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TranslationService } from './translation-service.service';
import { UtilityService } from './utility.service';
import { Router } from '@angular/router';
import { SqliteService } from './sqlite.service';

@Injectable({
  providedIn: 'root'
})
export class ShipmentService implements IService {

  constructor( 
    private loadingService: LoadingService,
    public translation: TranslationService,
    public utility: UtilityService,
    public router: Router,
    public sqlite: SqliteService,
    private http:HttpClient) { }


  getAllThemes(): any[] {
    throw new Error("Method not implemented.");
  }
  getTitle(): string {
    throw new Error("Method not implemented.");
  }
  load(menuItem: any) {
    throw new Error("Method not implemented.");
  }

  getShipmentList(search = '', offset = 0): Promise<any> {

    return new Promise( async resolve => {

      let count = await this.sqlite.getShipmentCount();

      // logic to load data after 24 hours
      var time = localStorage.getItem('shipment_call');
      var callagain = false;
      if(!time){
        var ts = Math.round((new Date()).getTime() / 1000).toString();
        localStorage.setItem('shipment_call', ts );
        callagain = true;
      }

      var ntime = parseInt(localStorage.getItem('shipment_call'));
      
      if( ( (ntime % 1) / 3600) > 1 ){
        callagain = true;
      }


      
      console.log(count);
      if(count > 0 && !callagain){
        resolve(this.getListBySqlite(search, offset))
      }else{
        resolve(this.getListByUrl(search, offset))
      }

    })

    

    
  }

  getListBySqlite(search, offset){
    return new Promise( async resolve => {
      let obj = await this.sqlite.getShipmentInDatabase(search, offset);
      resolve(obj);
    })
  }

  insertDataInSqlite(data){
    return new Promise( async resolve => {
      await this.sqlite.setShipmentInDatabase(data);
      resolve(true);
    })
  }


  getListByUrl(search = '', offset = 0){

    return new Promise( resolve => {

      var url = environment.SERVER_URL+"/rest/shipments/list";
   
      var headers = new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      });

      let projectId = localStorage.getItem(environment.PROJECT_CODE);

      if(!projectId){

        this.translation.getTranslateKey("project_selection_error").then( v => {
          this.utility.presentFailureToast(v)
        })
        
        this.router.navigateByUrl('setting')
        return 
      }

      let request = {
        'username': localStorage.getItem(environment.USER_NAME),
        'token': localStorage.getItem(environment.TOKEN),
        'locale': 'en_US',
        'projectId':localStorage.getItem(environment.PROJECT_CODE),
        'statusCodes': localStorage.getItem(environment.STATUS_CODE).split(',')
        }
      this.http.post(url,request, {headers: headers}).subscribe( (d: any) => {
        console.log(d);

        let data = d.responseData.entities;
        this.insertDataInSqlite(data).then( () => {
          resolve(this.getListBySqlite(search, offset));
        })
      }, error => {
        let obj = {
          offset: -1,
          data: []
        }
        resolve(obj);
      });

    })
  }


}

