import { Injectable } from "@angular/core";
import { CapacitorSQLite, JsonSQLite } from "@capacitor-community/sqlite";
import {
  SQLite,
  SQLiteDatabaseConfig,
  SQLiteObject,
} from "@ionic-native/sqlite/ngx";
import { Platform } from "@ionic/angular";
import { NetworkService } from "./network.service";
import { UtilityService } from "./utility.service";
import { StorageService } from "./storage.service";
import { browserDBInstance } from "./browser-db-instance";
import { Capacitor } from "@capacitor/core";
import { error } from "protractor";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";

// const menu_json = require('../data/sidemenu.json');

declare var window: any;
const SQL_DB_NAME = "mcappSQLite.db";

@Injectable({
  providedIn: "root",
})
export class SqliteService {
  db: any;
  sqliteDB: SQLiteObject;
  DB_SETUP_KEY = "first_db_setup";
  batchSqlCmd;
  batchSqlCmdVals;
  resultingCmd;
  config: SQLiteDatabaseConfig = {
    name: "mcapp.db",
    location: "default",
  };

  public msg = "Sync In Progress ...";

  constructor(
    private storage: StorageService,
    private platform: Platform,
    private androidpermissions: AndroidPermissions
  ) {}

  public initialize() {
    return new Promise(async (resolve) => {
      if (Capacitor.getPlatform() != "web") await this.initializeDatabase();
      resolve(true);
    });
  }

  async initializeDatabase() {
    return new Promise(async (resolve) => {
      // initialize database object
      const flag = await this.createDatabase();

      if (!flag) {
        resolve(false);
      }

      await this.initializeUsersTable();
      // initialize the user flags table for screens
      await this.initializeSidemenuTable();
      // initialize users table

      await this.initializeLogTable();

      await this.initializeContainersTable();

      await this.initializeShipmentTable();

      await this.initializePackageTable();

      await this.initializeStockTable();

      resolve(true);
    });
  }

  async createDatabase() {
    return new Promise(async (resolve) => {
      if (Capacitor.getPlatform() != "web") {
        var self = this;
        const dbName = SQL_DB_NAME;
        const dbNames = [SQL_DB_NAME];
        const ret = await CapacitorSQLite.checkConnectionsConsistency({
          dbNames: dbNames,
        });
        if (ret.result === false) {
          await CapacitorSQLite.createConnection({ database: dbName });
          await CapacitorSQLite.open({ database: dbName });
          resolve(true);
        } else {
          await CapacitorSQLite.open({ database: dbName });
          resolve(true);
        }
      } else {
        let _db = window.openDatabase(
          SQL_DB_NAME,
          "1.0",
          "DEV",
          5 * 1024 * 1024
        );
        this.msg = "Database initialized";
        resolve(true);
      }
    });
  }

  async initializeUsersTable() {
    return new Promise(async (resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS users(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "accountId INTEGER DEFAULT 0, ";
      sql += "creationDate TEXT, ";
      sql += "email TEXT, ";
      sql += "firstname TEXT, ";
      sql += "industry INTEGER DEFAULT 0, ";
      sql += "lastname TEXT, ";
      sql += "phoneNumber TEXT, ";
      sql += "project TEXT, ";
      sql += "refreshToken TEXT, ";
      sql += "role_id INTEGER, ";
      sql += "role_name TEXT, ";
      sql += "startApi TEXT, ";
      sql += "success TEXT, ";
      sql += "token TEXT, ";
      sql += "userId INTEGER, ";
      sql += "username TEXT, ";
      sql += "active INTEGER DEFAULT 0 ";
      sql += ")";

      this.msg = "Initializing Users ...";

      let table = await this.execute(sql, []);
      resolve(table);
    });
  }

  async initializeSidemenuTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS sidemenu(";
      sql += "user_id INTEGER, ";
      sql += "title TEXT, ";
      sql += "route TEXT, ";
      sql += "url TEXT, ";
      sql += "theme TEXT, ";
      sql += "icon TEXT, ";
      sql += "listView BOOLEAN DEFAULT false, ";
      sql += "component TEXT, ";
      sql += "singlePage BOOLEAN DEFAULT false, ";
      sql += "toggle BOOLEAN DEFAULT false, ";
      sql += "orderpair INTEGER, ";
      sql += "UNIQUE(user_id, route)";

      sql += ")";

      this.msg = "Initializing User Menu ...";
      resolve(this.execute(sql, []));
    });
  }

  async initializeFlagsTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS user_flags(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "dashboard BOOLEAN DEFAULT false, ";
      sql += "sync_contact_from_phone BOOLEAN DEFAULT false, ";
      sql += "sync_contact_from_zuul BOOLEAN DEFAULT false, ";
      sql += "google_map BOOLEAN DEFAULT false, ";
      sql += "active_pass_list BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_archive BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_sent BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_scanned BOOLEAN DEFAULT false, ";
      sql += "sent_pass_list BOOLEAN DEFAULT false, ";
      sql += "pass_details BOOLEAN DEFAULT false, ";
      sql += "notifications BOOLEAN DEFAULT false, ";
      sql += "request_a_pass BOOLEAN DEFAULT false, ";
      sql += "create_new_pass BOOLEAN DEFAULT false )";

      this.msg = "Initializing User Flags ...";
      resolve(this.execute(sql, []));
    });
  }

  async initializeLogTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS logs(";
      sql += "url text, ";
      sql += "type text, ";
      sql += "sent text, ";
      sql += "data text, ";
      sql += "timestamp )";

      this.msg = "Initializing User Flags ...";
      resolve(this.execute(sql, []));
    });
  }

  // Container Data
  async initializeContainersTable() {
    return new Promise(async (resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS Containers(";

      sql += "areaNumbers text, ";
      sql += "containerCode text, ";
      sql += "containerNo text, ";
      sql += "containerTypeId text, ";
      sql += "containerValueId text, ";
      sql += "description text, ";
      sql += "finalStatusId integer, ";
      sql += "freeText text, ";
      sql += "gross integer, ";
      sql += "height text, ";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "itemQuantity text, ";
      sql += "length text, ";
      sql += "location text, ";
      sql += "net integer, ";
      sql += "poNo text, ";
      sql += "progress text, ";
      sql += "projectId text, ";
      sql += "receivedDate text, ";
      sql += "receiverDetails text, ";
      sql += "receiverId text, ";
      sql += "receiverName text, ";
      sql += "sealNo text, ";
      sql += "sendMail text, ";
      sql += "shipmentNo text, ";
      sql += "shipmentStatus text, ";
      sql += "status text, ";
      sql += "supplierName text, ";
      sql += "tare text, ";
      sql += "total integer, ";
      sql += "totalPackageWeight text, ";
      sql += "type text, ";
      sql += "updateSubjects text, ";
      sql += "width text ";

      sql += ")";

      this.msg = "Initializing User Flags ...";
      let table = await this.execute(sql, []);
      resolve(table);
    });
  }

  public async getContainersCount() {
    return new Promise(async (resolve) => {
      let sql = "SELECT COUNT(*) FROM Containers";
      let values = [];

      let d = await this.execute(sql, values);
      console.log(d);
      if (!d) {
        resolve(0);
        return;
      }
      const data = this.getRows(d);
      if (data.length == 0) {
        resolve(0);
        return;
      }
      resolve(data[0]["COUNT(*)"]);
    });
  }

  public async deleteAllContainers() {
    return new Promise(async (resolve) => {
      let sql = "DELETE FROM Containers";
      let values = [];

      let d = await this.execute(sql, values);
      resolve(d);
    });
  }

  public async setContainersInDatabase(data) {
    return new Promise(async (resolve) => {
      await this.deleteAllContainers();

      let insertRows = [];

      for (var i = 0; i < data.length; i++) {
        // get sidemenu json data from file and dump into sqlite table

        var sql = "INSERT OR REPLACE INTO Containers(";

        sql += "areaNumbers, ";
        sql += "containerCode, ";
        sql += "containerNo, ";
        sql += "containerTypeId, ";
        sql += "containerValueId, ";
        sql += "description, ";
        sql += "finalStatusId, ";
        sql += "freeText, ";
        sql += "gross, ";
        sql += "height, ";
        sql += "id, ";
        sql += "itemQuantity, ";
        sql += "length, ";
        sql += "location, ";
        sql += "net, ";
        sql += "poNo, ";
        sql += "progress, ";
        sql += "projectId, ";
        sql += "receivedDate, ";
        sql += "receiverDetails, ";
        sql += "receiverId, ";
        sql += "receiverName, ";
        sql += "sealNo, ";
        sql += "sendMail, ";
        sql += "shipmentNo, ";
        sql += "shipmentStatus, ";
        sql += "status, ";
        sql += "supplierName, ";
        sql += "tare, ";
        sql += "total, ";
        sql += "totalPackageWeight, ";
        sql += "type, ";
        sql += "updateSubjects, ";
        sql += "width ";

        sql += ") ";

        sql += "VALUES (";

        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "? ";
        sql += ")";

        var values = [
          data[i]["areaNumbers"],
          data[i]["containerCode"],
          data[i]["containerNo"],
          data[i]["containerTypeId"],
          data[i]["containerValueId"],
          data[i]["description"],
          data[i]["finalStatusId"],
          data[i]["freeText"],
          data[i]["gross"],
          data[i]["height"],
          data[i]["id"],
          data[i]["itemQuantity"],
          data[i]["length"],
          data[i]["location"],
          data[i]["net"],
          data[i]["poNo"],
          data[i]["progress"],
          data[i]["projectId"],
          data[i]["receivedDate"],
          data[i]["receiverDetails"],
          data[i]["receiverId"],
          data[i]["receiverName"],
          data[i]["sealNo"],
          data[i]["sendMail"],
          data[i]["shipmentNo"],
          data[i]["shipmentStatus"],
          data[i]["status"],
          data[i]["supplierName"],
          data[i]["tare"],
          data[i]["total"],
          data[i]["totalPackageWeight"],
          data[i]["type"],
          data[i]["updateSubjects"],
          data[i]["width"],
        ];

        // await this.execute(sql, values);
        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  public async getContainersInDatabase(
    search = null,
    offset = 0,
    loader = false
  ) {
    return new Promise(async (resolve) => {
      var sql = "SELECT * FROM Containers ";
      var values = [];

      if (search) {
        sql +=
          "where containerNo like ? OR location like ? OR progress like ? ";
        values.push("%" + search + "%", "%" + search + "%", "%" + search + "%");
      }

      sql += " ORDER BY containerNo ASC limit ? OFFSET ?";
      values.push(30, offset);

      let d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        let obj = {
          offset: -1,
          data: [],
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {
        offset = data.length < 30 ? -1 : offset + 30;

        let obj = {
          offset: offset,
          data: data,
        };
        resolve(obj);
      } else {
        let obj = {
          offset: -1,
          data: [],
        };
        resolve(obj);
      }
    });
  }

  // Stock

  async initializeStockTable() {
    return new Promise(async (resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS Stock(";

      sql += "id integer PRIMARY KEY,";
      sql += "available integer,";
      sql += "description text,";
      sql += "description2 text,";
      sql += "drawingNo text,";
      sql += "freeText text,";
      sql += "locationString text,";
      sql += "packageNo text,";
      sql += "partNo text,";
      sql += "poNo text,";
      sql += "posNo text,";
      sql += "productCode text,";
      sql += "productGroup text,";
      sql += "productNumber text,";
      sql += "productType text,";
      sql += "purchasePrice integer,";
      sql += "quantity integer,";
      sql += "reserved integer,";
      sql += "salesPrice integer,";
      sql += "unit text,";
      sql += "vat integer,";
      sql += "workNumber text";
      sql += ")";

      this.msg = "Initializing User Flags ...";

      let table = await this.execute(sql, []);
      resolve(table);
    });
  }

  public async getStockCount() {
    return new Promise(async (resolve) => {
      let sql = "SELECT COUNT(*) FROM Stock";
      let values = [];

      let d = await this.execute(sql, values);
      console.log(d);
      if (!d) {
        resolve(0);
        return;
      }
      const data = this.getRows(d);
      if (data.length == 0) {
        resolve(0);
        return;
      }
      resolve(data[0]["COUNT(*)"]);
    });
  }

  public async deleteAllStock() {
    return new Promise(async (resolve) => {
      let sql = "DELETE FROM Stock";
      let values = [];

      let d = await this.execute(sql, values);
      resolve(d);
    });
  }

  public async setStockInDatabase(data) {
    return new Promise(async (resolve) => {
      await this.deleteAllStock();

      let insertRows = [];
      console.log(data);
      for (var i = 0; i < data.length; i++) {
        // get sidemenu json data from file and dump into sqlite table

        var sql = "INSERT OR REPLACE INTO Stock(";

        sql += "id,";
        sql += "available,";
        sql += "description,";
        sql += "description2,";
        sql += "drawingNo,";
        sql += "freeText,";
        sql += "locationString,";
        sql += "packageNo,";
        sql += "partNo,";
        sql += "poNo,";
        sql += "posNo,";
        sql += "productCode,";
        sql += "productGroup,";
        sql += "productNumber,";
        sql += "productType,";
        sql += "purchasePrice,";
        sql += "quantity,";
        sql += "reserved,";
        sql += "salesPrice,";
        sql += "unit,";
        sql += "vat,";
        sql += "workNumber";

        sql += ") ";

        sql += "VALUES (";

        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?";
        sql += ")";

        var values = [
          data[i]["id"] ? data[i]["id"] : Date.now(),
          data[i]["available"] ? data[i]["available"] : 0,
          data[i]["description"] ? data[i]["description"] : "",
          data[i]["description2"] ? data[i]["description2"] : "",
          data[i]["drawingNo"] ? data[i]["drawingNo"] : "",
          data[i]["freeText"] ? data[i]["freeText"] : "",
          data[i]["locationString"] ? data[i]["locationString"] : "",
          data[i]["packageNo"] ? data[i]["packageNo"] : "",
          data[i]["partNo"] ? data[i]["partNo"] : "",
          data[i]["poNo"] ? data[i]["poNo"] : "",
          data[i]["posNo"] ? data[i]["posNo"] : "",
          data[i]["productCode"] ? data[i]["productCode"] : "",
          data[i]["productGroup"] ? data[i]["productGroup"] : "",
          data[i]["productNumber"] ? data[i]["productNumber"] : "",
          data[i]["productType"] ? data[i]["productType"] : "",
          data[i]["purchasePrice"] ? data[i]["purchasePrice"] : 0,
          data[i]["quantity"] ? data[i]["quantity"] : 1,
          data[i]["reserved"] ? data[i]["reserved"] : 0,
          data[i]["salesPrice"] ? data[i]["salesPrice"] : 0,
          data[i]["unit"] ? data[i]["unit"] : "",
          data[i]["vat"] ? data[i]["vat"] : 0,
          data[i]["workNumber"] ? data[i]["workNumber"] : "",
        ];

        // await this.execute(sql, values);
        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  public async getStockInDatabase(search = null, offset = 0, loader = false) {
    return new Promise(async (resolve) => {
      var sql = "SELECT * FROM Stock ";
      var values = [];

      if (search) {
        sql +=
          "where posNo like ? OR description like ? OR locationString like ? ";
        values.push("%" + search + "%", "%" + search + "%", "%" + search + "%");
      }

      sql += " ORDER BY posNo ASC limit ? OFFSET ?";
      values.push(30, offset);

      let d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        let obj = {
          offset: -1,
          data: [],
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {
        offset = data.length < 30 ? -1 : offset + 30;

        let obj = {
          offset: offset,
          data: data,
        };
        resolve(obj);
      } else {
        let obj = {
          offset: -1,
          data: [],
        };
        resolve(obj);
      }
    });
  }

  // Packages

  async initializePackageTable() {
    return new Promise(async (resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS Package(";

      sql += "id integer,";
      sql += "packageTypeId text,";
      sql += "storageTypeId text,";
      sql += "transportModeId text,";
      sql += "packageGroupId text,";
      sql += "projectAreaId text,";
      sql += "finalStatusId text,";
      sql += "projectId text,";
      sql += "receiverId text,";
      sql += "status text,";
      sql += "packageNo text,";
      sql += "poNo text,";
      sql += "containerNo text,";
      sql += "description text,";
      sql += "progress text,";
      sql += "location text,";
      sql += "freeText text,";
      sql += "area text,";
      sql += "supplierText text,";
      sql += "shipmentStatus text,";
      sql += "workNo text,";
      sql += "storageTypeName text,";
      sql += "groupName text,";
      sql += "packageTypeName text,";
      sql += "receiverName text,";
      sql += "receiverDetails text,";
      sql += "net text,";
      sql += "gross text,";
      sql += "length text,";
      sql += "width text,";
      sql += "height text,";
      sql += "volume text,";
      sql += "receivedDate text,";
      sql += "stackability text,";
      sql += "sendMail text,";
      sql += "updateSubjects text";

      sql += ")";

      this.msg = "Initializing User Flags ...";
      let table = await this.execute(sql, []);
      resolve(table);
    });
  }

  public async getPackageCount() {
    return new Promise(async (resolve) => {
      let sql = "SELECT COUNT(*) FROM Package";
      let values = [];

      let d = await this.execute(sql, values);
      console.log(d);
      if (!d) {
        resolve(0);
        return;
      }
      const data = this.getRows(d);
      if (data.length == 0) {
        resolve(0);
        return;
      }
      resolve(data[0]["COUNT(*)"]);
    });
  }

  public async deleteAllPackages() {
    return new Promise(async (resolve) => {
      let sql = "DELETE FROM Package";
      let values = [];

      let d = await this.execute(sql, values);
      resolve(d);
    });
  }

  public async setPackageInDatabase(data) {
    return new Promise(async (resolve) => {
      await this.deleteAllPackages();

      let insertRows = [];

      for (var i = 0; i < data.length; i++) {
        // get sidemenu json data from file and dump into sqlite table

        var sql = "INSERT OR REPLACE INTO Package(";

        sql += "id,";
        sql += "packageTypeId,";
        sql += "storageTypeId,";
        sql += "transportModeId,";
        sql += "packageGroupId,";
        sql += "projectAreaId,";
        sql += "finalStatusId,";
        sql += "projectId,";
        sql += "receiverId,";
        sql += "status,";
        sql += "packageNo,";
        sql += "poNo,";
        sql += "containerNo,";
        sql += "description,";
        sql += "progress,";
        sql += "location,";
        sql += "freeText,";
        sql += "area,";
        sql += "supplierText,";
        sql += "shipmentStatus,";
        sql += "workNo,";
        sql += "storageTypeName,";
        sql += "groupName,";
        sql += "packageTypeName,";
        sql += "receiverName,";
        sql += "receiverDetails,";
        sql += "net,";
        sql += "gross,";
        sql += "length,";
        sql += "width,";
        sql += "height,";
        sql += "volume,";
        sql += "receivedDate,";
        sql += "stackability,";
        sql += "sendMail,";
        sql += "updateSubjects ";

        sql += ") ";

        sql += "VALUES (";

        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?,";
        sql += "?";
        sql += ")";

        var values = [
          data[i]["id"],
          data[i]["packageTypeId"],
          data[i]["storageTypeId"],
          data[i]["transportModeId"],
          data[i]["packageGroupId"],
          data[i]["projectAreaId"],
          data[i]["finalStatusId"],
          data[i]["projectId"],
          data[i]["receiverId"],
          data[i]["status"],
          data[i]["packageNo"],
          data[i]["poNo"],
          data[i]["containerNo"],
          data[i]["description"],
          data[i]["progress"],
          data[i]["location"],
          data[i]["freeText"],
          data[i]["area"],
          data[i]["supplierText"],
          data[i]["shipmentStatus"],
          data[i]["workNo"],
          data[i]["storageTypeName"],
          data[i]["groupName"],
          data[i]["packageTypeName"],
          data[i]["receiverName"],
          data[i]["receiverDetails"],
          data[i]["net"],
          data[i]["gross"],
          data[i]["length"],
          data[i]["width"],
          data[i]["height"],
          data[i]["volume"],
          data[i]["receivedDate"],
          data[i]["stackability"],
          data[i]["sendMail"],
          data[i]["updateSubjects"],
        ];

        // await this.execute(sql, values);
        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  public async getPackageInDatabase(search = null, offset = 0, loader = false) {
    return new Promise(async (resolve) => {
      var sql = "SELECT * FROM Package ";
      var values = [];

      if (search) {
        sql += "where packageNo like ? OR location like ? OR progress like ? ";
        values.push("%" + search + "%", "%" + search + "%", "%" + search + "%");
      }

      sql += " ORDER BY packageNo ASC limit ? OFFSET ?";
      values.push(30, offset);

      let d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        let obj = {
          offset: -1,
          data: [],
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {
        offset = data.length < 30 ? -1 : offset + 30;

        let obj = {
          offset: offset,
          data: data,
        };
        resolve(obj);
      } else {
        let obj = {
          offset: -1,
          data: [],
        };
        resolve(obj);
      }
    });
  }

  // Shipments

  async initializeShipmentTable() {
    return new Promise(async (resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS Shipment(";

      sql += "areaNumbers text, ";
      sql += "containerCode text, ";
      sql += "containerNo text, ";
      sql += "containerTypeId text, ";
      sql += "containerValueId text, ";
      sql += "description text, ";
      sql += "finalStatusId integer, ";
      sql += "freeText text, ";
      sql += "gross integer, ";
      sql += "height text, ";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "itemQuantity text, ";
      sql += "length text, ";
      sql += "location text, ";
      sql += "net integer, ";
      sql += "poNo text, ";
      sql += "progress text, ";
      sql += "projectId text, ";
      sql += "receivedDate text, ";
      sql += "receiverDetails text, ";
      sql += "receiverId text, ";
      sql += "receiverName text, ";
      sql += "sealNo text, ";
      sql += "sendMail text, ";
      sql += "shipmentNo text, ";
      sql += "shipmentStatus text, ";
      sql += "status text, ";
      sql += "supplierName text, ";
      sql += "tare text, ";
      sql += "total integer, ";
      sql += "totalPackageWeight text, ";
      sql += "type text, ";
      sql += "updateSubjects text, ";
      sql += "width text ";

      sql += ")";

      this.msg = "Initializing User Flags ...";
      let table = await this.execute(sql, []);
      resolve(table);
    });
  }

  public async getShipmentCount() {
    return new Promise(async (resolve) => {
      let sql = "SELECT COUNT(*) FROM Shipment";
      let values = [];

      let d = await this.execute(sql, values);
      console.log(d);
      if (!d) {
        resolve(0);
        return;
      }
      const data = this.getRows(d);
      if (data.length == 0) {
        resolve(0);
        return;
      }
      resolve(data[0]["COUNT(*)"]);
    });
  }

  public async deleteAllShipment() {
    return new Promise(async (resolve) => {
      let sql = "DELETE FROM Shipment";
      let values = [];

      let d = await this.execute(sql, values);
      resolve(d);
    });
  }

  public async setShipmentInDatabase(data) {
    return new Promise(async (resolve) => {
      await this.deleteAllPackages();

      let insertRows = [];

      for (var i = 0; i < data.length; i++) {
        // get sidemenu json data from file and dump into sqlite table

        var sql = "INSERT OR REPLACE INTO Shipment(";

        sql += "areaNumbers, ";
        sql += "containerCode, ";
        sql += "containerNo, ";
        sql += "containerTypeId, ";
        sql += "containerValueId, ";
        sql += "description, ";
        sql += "finalStatusId, ";
        sql += "freeText, ";
        sql += "gross, ";
        sql += "height, ";
        sql += "id, ";
        sql += "itemQuantity, ";
        sql += "length, ";
        sql += "location, ";
        sql += "net, ";
        sql += "poNo, ";
        sql += "progress, ";
        sql += "projectId, ";
        sql += "receivedDate, ";
        sql += "receiverDetails, ";
        sql += "receiverId, ";
        sql += "receiverName, ";
        sql += "sealNo, ";
        sql += "sendMail, ";
        sql += "shipmentNo, ";
        sql += "shipmentStatus, ";
        sql += "status, ";
        sql += "supplierName, ";
        sql += "tare, ";
        sql += "total, ";
        sql += "totalPackageWeight, ";
        sql += "type, ";
        sql += "updateSubjects, ";
        sql += "width ";

        sql += ") ";

        sql += "VALUES (";

        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "? ";
        sql += ")";

        var values = [
          data[i]["areaNumbers"],
          data[i]["containerCode"],
          data[i]["containerNo"],
          data[i]["containerTypeId"],
          data[i]["containerValueId"],
          data[i]["description"],
          data[i]["finalStatusId"],
          data[i]["freeText"],
          data[i]["gross"],
          data[i]["height"],
          data[i]["id"],
          data[i]["itemQuantity"],
          data[i]["length"],
          data[i]["location"],
          data[i]["net"],
          data[i]["poNo"],
          data[i]["progress"],
          data[i]["projectId"],
          data[i]["receivedDate"],
          data[i]["receiverDetails"],
          data[i]["receiverId"],
          data[i]["receiverName"],
          data[i]["sealNo"],
          data[i]["sendMail"],
          data[i]["shipmentNo"],
          data[i]["shipmentStatus"],
          data[i]["status"],
          data[i]["supplierName"],
          data[i]["tare"],
          data[i]["total"],
          data[i]["totalPackageWeight"],
          data[i]["type"],
          data[i]["updateSubjects"],
          data[i]["width"],
        ];

        // await this.execute(sql, values);
        insertRows.push([sql, values]);
      }

      var size = 250;
      var arrayOfArrays = [];

      for (var i = 0; i < insertRows.length; i += size) {
        arrayOfArrays.push(insertRows.slice(i, i + size));
      }

      console.log(arrayOfArrays);

      for (var j = 0; j < arrayOfArrays.length; j++) {
        await this.executeBatch(arrayOfArrays[j]);
        // await this.execute(s, p)
      }

      resolve(true);
    });
  }

  public async getShipmentInDatabase(
    search = null,
    offset = 0,
    loader = false
  ) {
    return new Promise(async (resolve) => {
      var sql = "SELECT * FROM Shipment ";
      var values = [];

      if (search) {
        sql += "where shipmentNo like ? OR location like ? OR progress like ? ";
        values.push("%" + search + "%", "%" + search + "%", "%" + search + "%");
      }

      sql += " ORDER BY shipmentNo ASC limit ? OFFSET ?";
      values.push(30, offset);

      let d = await this.execute(sql, values);
      // var data = d as any[];
      if (!d) {
        let obj = {
          offset: -1,
          data: [],
        };
        resolve(obj);
        return;
      }
      const data = this.getRows(d);

      if (data.length > 0) {
        offset = data.length < 30 ? -1 : offset + 30;

        let obj = {
          offset: offset,
          data: data,
        };
        resolve(obj);
      } else {
        let obj = {
          offset: -1,
          data: [],
        };
        resolve(obj);
      }
    });
  }

  // LOG QUERY START

  public async setLogInDatabase(log) {
    return new Promise(async (resolve) => {
      // get sidemenu json data from file and dump into sqlite table

      var sql = "INSERT OR REPLACE INTO logs(";

      sql += "url, ";
      sql += "type, ";
      sql += "sent, ";
      sql += "data, ";
      sql += "timestamp ";

      sql += ") ";

      sql += "VALUES (";

      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "? ";
      sql += ")";

      var values = [
        log["url"],
        log["type"],
        log["sent"],
        log["data"],
        log["timestamp"],
      ];

      await this.execute(sql, values);
      resolve(true);
    });
  }

  public async getLogInDatabase() {
    return new Promise(async (resolve) => {
      let sql = "SELECT * FROM logs order by timestamp desc";
      let values = [];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  public async clearLogInDatabase() {
    return new Promise(async (resolve) => {
      let sql = "DELETE FROM logs";
      let values = [];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  // LOG QUERY ENDS

  public async setUserInDatabase(_user) {
    return new Promise(async (resolve) => {
      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      var sql = "INSERT OR REPLACE INTO users(";
      sql += "id, ";
      sql += "accountId, ";
      sql += "creationDate, ";
      sql += "email, ";
      sql += "firstname, ";
      sql += "industry, ";
      sql += "lastname, ";
      sql += "phoneNumber, ";
      sql += "project, ";
      sql += "refreshToken, ";
      sql += "role_id, ";
      sql += "role_name, ";
      sql += "startApi, ";
      sql += "userId, ";
      sql += "username ";
      sql += ") ";

      sql += "VALUES (";

      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "? "; // 15
      sql += ")";

      var values = [
        _user.id,
        _user.accountId,
        _user.creationDate,
        _user.email,
        _user.firstname,
        _user.industry,
        _user.lastname,
        _user.phoneNumber,
        _user.project,
        _user.refreshToken,
        _user.role.id,
        _user.role.name,
        _user.startApi,
        _user.userId,
        _user.username,
      ];

      await this.execute(sql, values);

      if (_user.token) {
        let sql3 = "UPDATE users SET active = ?";
        let values3 = [0];
        await this.execute(sql3, values3);

        let sql2 = "UPDATE users SET token = ?, active = ? where id = ?";
        let values2 = [_user.token, 1, _user.id];

        await this.execute(sql2, values2);
      }

      resolve(await this.getActiveUser());
    });
  }

  public async getUserSidemenu(withtoggle = false) {
    let user_id = await this.getActiveUserId();
    return new Promise(async (resolve) => {
      // check if a user sidemenu exist ... if yes return that one ... else dump a side menu and return it
      let sql = "SELECT * FROM sidemenu where user_id = ?";
      let values = [user_id];

      if (withtoggle) {
        sql += " and toggle = ?";
        values.push(true);
      }

      sql += " order by orderpair asc";

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(this.setSidemenuWithDefaultValues(user_id, withtoggle));
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve(this.setSidemenuWithDefaultValues(user_id, withtoggle));
      }
    });
  }

  public async setSidemenuWithDefaultValues(user_id, withtoggle = false) {
    return new Promise(async (resolve) => {
      // get sidemenu json data from file and dump into sqlite table
      let sidemenu = []; // menu_json;
      for (var i = 0; i < sidemenu.length; i++) {
        var sql = "INSERT OR REPLACE INTO sidemenu(";
        sql += "user_id, ";
        sql += "title, ";
        sql += "route, ";
        sql += "url, ";
        sql += "theme, ";
        sql += "icon, ";
        sql += "listView, ";
        sql += "component, ";
        sql += "singlePage, ";
        sql += "toggle, ";
        sql += "orderpair ";
        sql += ") ";

        sql += "VALUES (";

        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "? "; // 11
        sql += ")";

        var values = [
          user_id,
          sidemenu[i]["title"],
          sidemenu[i]["route"],
          sidemenu[i]["url"],
          sidemenu[i]["theme"],
          sidemenu[i]["icon"],
          sidemenu[i]["listView"],
          sidemenu[i]["component"],
          sidemenu[i]["singlePage"],
          sidemenu[i]["toggle"],
          sidemenu[i]["orderpair"],
        ];

        await this.execute(sql, values);
      }

      resolve(this.getSidemenuByUserId(user_id, withtoggle));
    });
  }

  public async setSidemenuItem(menu) {
    return new Promise(async (resolve) => {
      // get sidemenu json data from file and dump into sqlite table

      var sql = "INSERT OR REPLACE INTO sidemenu(";
      sql += "user_id, ";
      sql += "title, ";
      sql += "route, ";
      sql += "url, ";
      sql += "theme, ";
      sql += "icon, ";
      sql += "listView, ";
      sql += "component, ";
      sql += "singlePage, ";
      sql += "toggle, ";
      sql += "orderpair ";
      sql += ") ";

      sql += "VALUES (";

      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "? "; // 11
      sql += ")";

      var values = [
        menu["user_id"],
        menu["title"],
        menu["route"],
        menu["url"],
        menu["theme"],
        menu["icon"],
        menu["listView"],
        menu["component"],
        menu["singlePage"],
        menu["toggle"],
        menu["orderpair"],
      ];

      await this.execute(sql, values);

      resolve(menu);
    });
  }

  public async getSidemenuByUserId(user_id, withtoggle = false) {
    return new Promise(async (resolve) => {
      let sql = "SELECT * FROM sidemenu where user_id = ?";
      let values = [user_id];

      if (withtoggle) {
        sql += " and toggle = ?";
        values.push(true);
      }

      sql += " order by orderpair asc";

      // let sql = "SELECT * FROM sidemenu where user_id = ? ORDER BY orderpair asc";
      // let values = [id];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve(null);
      }
    });
  }

  public async getCurrentUserAuthorizationToken() {
    return new Promise(async (resolve) => {
      let user_id = await this.getActiveUserId();
      let sql = "SELECT token FROM users where id = ? limit 1";
      let values = [user_id];

      let d = await this.execute(sql, values);
      // this.utility.presentToast(d);
      if (!d) {
        resolve(null);
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data[0]["token"]);
      } else {
        resolve(null);
      }
    });
  }

  public async setUserActiveById(id) {
    return new Promise(async (resolve) => {
      let sql3 = "UPDATE users SET active = ?";
      let values3 = [0];
      await this.execute(sql3, values3);

      let sql2 = "UPDATE users SET active = ? where id = ?";
      let values2 = [1, id];
      await this.execute(sql2, values2);

      resolve(this.getUserById(id));
    });
  }

  public async getUserById(id) {
    return new Promise(async (resolve) => {
      let sql = "SELECT * FROM users where id = ?";
      let values = [id];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        let id = data[0];
        resolve(id);
      } else {
        resolve(null);
      }
    });
  }

  public async getActiveUserId() {
    return new Promise(async (resolve) => {
      let sql = "SELECT id FROM users where active = ?";
      let values = [1];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        let id = data[0]["id"];
        resolve(id);
      } else {
        resolve(null);
      }
    });
  }

  public async getActiveUser() {
    return new Promise(async (resolve) => {
      let sql = "SELECT * FROM users where active = ? ";
      let values = [1];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        var user = data[0];
        resolve(user);
      } else {
        resolve(null);
      }
    });
  }

  setLogout() {
    return new Promise(async (resolve) => {
      let user_id = await this.getActiveUserId();

      let sql = "UPDATE users SET token = ?, active = ? where id = ?";
      let values = [null, 0, user_id];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }

  execute(sql, params) {
    return new Promise(async (resolve) => {
      return CapacitorSQLite.query({
        database: SQL_DB_NAME,
        statement: sql,
        values: params,
      })
        .then((response) => {
          console.log(response);
          resolve(response);
        })
        .catch((err) => {
          console.error(err);
          resolve(null);
        });
    });
  }

  prepareBatch(insertRows) {
    return new Promise(async (resolve) => {
      var size = 250;
      console.log(insertRows);
      var arrayOfArrays = [];

      for (var i = 0; i < insertRows.length; i += size) {
        arrayOfArrays.push(insertRows.slice(i, i + size));
      }

      console.log(arrayOfArrays);

      for (var j = 0; j < arrayOfArrays.length; j++) {
        await this.executeBatch(arrayOfArrays[j]);
        // await this.execute(s, p)
      }

      console.log(arrayOfArrays);

      resolve(true);
    });
  }

  executeBatch(array) {
    return new Promise(async (resolve) => {
      // if (!this.db) {
      //   await this.platform.ready();
      //   // initialize database object
      //   await this.createDatabase();
      // }

      let command = array[0][0];

      // let values = array[0][1];

      // let commandArray = command.split("VALUES");

      // let cmdString = commandArray[1].split(',');
      // console.log(cmdString);
      // console.log(commandArray);
      // for(let i = 0; i < cmdString.length; i++){
      //     cmdString[i] = values[i];
      // }

      // console.log("cmdString",cmdString);

      if (!command) {
        resolve(null);
      }

      let cmd = command.split("VALUES")[0] + "VALUES ";
      let values = [];

      for (let i = 0; i < array.length; i++) {
        let extractedArray = array[i];
        let brackets = array[i][0].split("VALUES")[1];
        cmd += brackets + (i != array.length - 1 ? ", " : "");

        console.log(array[i]);
        values = [...values, ...array[i][1]];
      }

      console.log("batch sql cmd: ", cmd);
      console.log("batch sql cmd 00 : ", values);

      return CapacitorSQLite.query({
        database: SQL_DB_NAME,
        statement: cmd,
        values: values,
      })
        .then((response) => {
          console.log("Response:", { response });
          resolve(response);
        })
        .catch((err) => {
          console.error(err);
          resolve(null);
        });
    });
  }

  private setValue(k, v) {
    return new Promise((resolve) => {
      this.storage.setKey(k, v).then(() => {
        resolve({ k: v });
      });
    });
  }

  private getValue(k): Promise<any> {
    return new Promise((resolve) => {
      this.storage.getKey(k).then((r) => {
        resolve(r);
      });
    });
  }

  private getRows(data) {
    var items = [];
    for (let i = 0; i < data.values.length; i++) {
      let item = data.values[i];

      items.push(item);
    }

    return items;
  }
}
