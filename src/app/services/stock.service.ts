import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SqliteService } from './sqlite.service';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private http:HttpClient, public sqlite: SqliteService,) { }

  searchStock(filters:any) : Observable<any> {
    var url = environment.SERVER_URL+"/rest/stockitems/list";
   
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    });

    let request = { 
      'username': localStorage.getItem(environment.USER_NAME),
      'token': localStorage.getItem(environment.TOKEN),
      ...filters,
      'isMobile':true      
    }
    return this.http.post(url,request, {headers: headers});

  }

  getStockList(search = '', offset = 0): Promise<any> {

    return new Promise( async resolve => {

      // let count = await this.sqlite.getContainersCount();
      // // logic to load data after 24 hours
      // var time = localStorage.getItem('stock_call');
      // var callagain = false;
      // if(!time){
      //   var ts = Math.round((new Date()).getTime() / 1000).toString();
      //   localStorage.setItem('container_call', ts );
      //   callagain = true;
      // }

      // var ntime = parseInt(localStorage.getItem('container_call'));
      
      // if( ( (ntime % 1) / 3600) > 1 ){
      //   callagain = true;
      // }


      
      // console.log(count);
      // if(count > 0 && !callagain){
        resolve(this.getStockListBySqlite(search, offset))
      // }else{
      //   resolve(this.getContainerListByUrl(search, offset))
      // }

    })

    

    
  }

  getStockListBySqlite(search, offset){
    return new Promise( async resolve => {
      let obj = await this.sqlite.getStockInDatabase(search, offset);
      resolve(obj);
    })
  }

  insertStockDataInSqlite(data){
    return new Promise( async resolve => {
      console.log(data)
      await this.sqlite.setStockInDatabase(data);
      resolve(true);
    })
  }


  getStockListByUrl(filters){

    return new Promise( resolve => {

      this.searchStock(filters).subscribe( v => {

        let data = v.responseData.stockitems;
        this.insertStockDataInSqlite(data).then( () => {
          resolve(this.getStockListBySqlite(null, 0));
        })

      }, error => {

        let obj = {
          offset: -1,
          data: []
        }
        resolve(obj);

      })

    })
  }

  

}
