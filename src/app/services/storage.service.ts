import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private nativeStorage: NativeStorage, public platform: Platform) { 

  }

  setKey(key, value): Promise<any>{

    return new Promise(resolve => {
      // if(this.platform.is('cordova')){
      //   this.nativeStorage.setItem(key, value)
      //   .then( () => resolve(true), error => { console.error('Error storing item', error), resolve(null) });
      // }else{
        resolve(localStorage.setItem(key, JSON.stringify(value)))  
      // }
      
    })

  }

  getKey(key): Promise<any>{
    
    return new Promise(resolve => {
      // if(this.platform.is('cordova')){
      //   this.nativeStorage.getItem('myitem')
      //   .then(data => resolve(data), error => resolve(null));
      // }else{
        let v = JSON.parse(localStorage.getItem(key));

        if (typeof v === 'string' || v instanceof String){
          v = v.replace(/['"]+/g, '');
        }
        resolve(v)
      // }
      
    })

  } 

  setUserToken(token){
    this.setKey('token', token);
  }  

  getUserToken(){
    return this.getKey('token');
  }

  setUserData(data){
    this.setKey('userdata', data);
    this.setUserToken(data['token']);
  }  


}
