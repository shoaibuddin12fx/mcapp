import { Injectable } from '@angular/core';
import { AlertsService } from './basic/alerts.service';
import { GeolocationsService } from './geolocations.service';
import { LoadingService } from './loading-service';
// import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  
  constructor(public loading: LoadingService, 
    public geolocations: GeolocationsService,
    // private launchNavigator: LaunchNavigator,
    public alerts: AlertsService,) { }
    showSearch:boolean;
  showLoader(msg = '') {
    return this.loading.showLoader(msg);
  }

  hideLoader() {
    return this.loading.hideLoader();
  }

  showAlert(msg) {
    return this.alerts.showAlert(msg);
  }

  presentToast(msg) {
    return this.alerts.presentToast(msg);
  }

  presentSuccessToast(msg) {
    return this.alerts.presentSuccessToast(msg);
  }

  presentFailureToast(msg) {
    return this.alerts.presentFailureToast(msg);
  }

  presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = ''): Promise<boolean> {
    return this.alerts.presentConfirm(okText = okText, cancelText = cancelText, title = title, message = message);
  }

  /* Geolocations */

  getCoordsForGeoAddress(address, _default = true) {
    return this.geolocations.getCoordsForGeoAddress(address, _default = true)
  }

  getCoordsViaHTML5Navigator() {
    return this.geolocations.getCoordsViaHTML5Navigator();
  }

  getCurrentLocationCoordinates() {
    return this.geolocations.getCurrentLocationCoordinates();
  }

  checkLocations(){
    return this.geolocations.checkLocations();
  }

  public async getCurrentLocation(lat, long) {

    var self = this;
    //this.storage.get('current_location').then((loc) => {
    let coords = await this.geolocations.getCurrentLocationCoordinates();

    var lt = coords['lat']
    var lg = coords['lng']
    // console.log(lt,lg);
    // var lt = (lat) ? lat : 0;
    // var lg = (long) ? long : 0;
    // var coords = {lat: lt, lng: lg};
    // this.launchNavigator.availableApps()
    //   .then(data => {

    //     if (data['google_maps']) {
    //       var options: LaunchNavigatorOptions = {
    //         start: [lt, lg],
    //         app: this.launchNavigator.APP.GOOGLE_MAPS
    //       };
    //       this.launchNavigator.navigate([lat, long], options)
    //     } else if (data['apple_maps']) {
    //       options = {
    //         start: [lt, lg],
    //         app: this.launchNavigator.APP.APPLE_MAPS
    //       };
    //       this.launchNavigator.navigate([lat, long], options)

    //     } else {
    //       self.presentToast("No Map software available");
    //     }

    //   })
      .catch(err => {
        self.presentToast(err);
      });


    // });


  }

  removeNullStringFromString(str){
    return str ? str.toLowerCase().replace('null', '') : str;
  }


  toggleSearch(search){
    this.showSearch = search;
  }

  gettoggleSearchValue(){
    return this.showSearch;
  }

  
}
