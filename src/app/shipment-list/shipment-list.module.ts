import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShipmentListPageRoutingModule } from './shipment-list-routing.module';

import { ShipmentListPage } from './shipment-list.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShipmentListPageRoutingModule,
    TranslateModule
  ],
  declarations: [ShipmentListPage]
})
export class ShipmentListPageModule {}
