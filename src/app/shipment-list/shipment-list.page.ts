import { Component, OnInit } from '@angular/core';
import { ContainerService } from '../services/container.service';
import { FormControl } from '@angular/forms';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { UtilityService } from '../services/utility.service';
import { ShipmentService } from '../services/shipment.service';

@Component({
  selector: 'app-shipment-list',
  templateUrl: './shipment-list.page.html',
  styleUrls: ['./shipment-list.page.scss'],
  providers: [ShipmentService]
})
export class ShipmentListPage implements OnInit {

  data: any;
  tempData: any;
  type: string;
  searchControl: FormControl;
  searching: any = false;
  itemClicked: boolean = false;
  tempArray: any = [];
  offset = 0;
  search = '';
  showSpinner = false;

  
  
  constructor(

    private service: ShipmentService, 
    private commonService: CommonService,
    public utility: UtilityService,
    private router: Router

  ) {
    this.getShipmentDetails();
  }

  async getShipmentDetails() {

    this.showSpinner = true;
    let obj = await this.service.getShipmentList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;
    console.log("problem")
    this.showSpinner = false;


  }

  async doRefresh($event){
    this.offset = 0;
    let obj = await this.service.getShipmentList(this.search, this.offset );
    this.offset = obj.offset;
    this.data = this.data;
    $event.target.complete()
  }

  async loadData($event){

    let obj = await this.service.getShipmentList(this.search, this.offset );
    this.offset = obj.offset;
    this.data = [...this.data, ...obj.data];
    $event.target.complete()

  }

  ionViewWillEnter(){
    this.getShipmentDetails();
  }

  ngOnInit() {
    
  }

  async onSearchTerm($event) {

    console.log($event.target.value);
    this.search = $event.target.value;
    this.offset = 0;
    let obj = await this.service.getShipmentList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;
    // let searchVal = searchTerm.detail.value;
    // this.data = this.tempData;

    // if (searchVal && searchVal.trim() !== '') {

    //   this.data = this.data.filter(item => {
    //     return item.containerNo.indexOf(searchVal) > -1;
    //   });
    // }
  }
  containerItemClick(params): void {
    console.log(params);

    if (params.isChecked) {
      this.tempArray.push(params.id);
    } else {
      this.tempArray.splice(this.tempArray.indexOf(params.id), 1);
    }

    if (this.tempArray.length > 0) {
      this.itemClicked = true;
    } else {
      this.itemClicked = false;
    }
  }


  bookNow() {

    let checkedData = this.data.filter( x => x.isChecked == true )
    this.commonService.addContainerData(checkedData);
    this.router.navigateByUrl("book-container");
    
  }
}
